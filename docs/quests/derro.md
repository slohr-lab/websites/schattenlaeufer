## Allgemeines

| | | | |
| - | - | - | - |
| __Auftraggeber__  | [Ylsa](../npc/ylsa.md)                           | __Bekommen__  |  [Session 14](../log/session-14.md)  |
| __Ort__           | [Gracklstugh](../locations/gracklstugh.md)       | __Gelöst__    | [Session 20](../log/session-20.md)   |

## Beschreibung

Die Derros, ehemalige Sklaven der Duergar, planen wohl einen weiteren Aufstand. Wir sollen herausfinden wer dahinter steckt und alle gefundenen Informationen an Ylsa weiterreichen.

## Reward

Kostenloser Eskort an die Oberfläche

## Tasks

- <input type="checkbox" disabled checked />  Derro (Droki) identifiziert welcher sehr viele Waffen im Hafenviertel von der Gruppe gekauft 
- <input type="checkbox" disabled checked />  Mit Droki sprechen
- <input type="checkbox" disabled checked />  Anführer Uskvil identifiziert, in Whorlstone Tunnels
- <input type="checkbox" disabled  /> Mit Uskvil sprechen 
- <input type="checkbox" disabled checked />  Ylsa alle beweise übergeben und sie war zufrieden.
