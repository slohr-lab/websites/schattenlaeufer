## Allgemeines

| | | | |
| - | - | - | - |
| __Auftraggeber__  | [Hgraam](../npc/hgraam.md)                       | __Bekommen__  |  [Session 14](../log/session-14.md)  |
| __Ort__           | [Gracklstugh](../locations/gracklstugh.md)       | __Gelöst__    |  [Session 20](../log/session-20.md)   |

## Beschreibung

Der Wahn breitet sich immer mehr unter den Bewohnern der Stadt aus. Auch die Geschehnisse außerhalb von Gracklstught machen Hgraam sorgen. 

## Belohnung

- Stonespeaker Crystal

## Tasks

- <input type="checkbox" disabled checked />  [Sarith](../npc/sarith.md) wurde untersucht - sehr weit fortgeschrittener Wahn
- <input type="checkbox" disabled checked />  [Derendil](../npc/derendil.md) wurde untersucht - wahn hat neue - friedliche - Persönlichkeit erschaffen und alte Persönlich komplett unterdrückt.
- <input type="checkbox" disabled checked />  Kultisten identifiziert welchen wohl den Wahn mit Magie verstecken.
- <input type="checkbox" disabled checked />  Kultisten ausfinden machen in den Whorlstone Tunnels
- <input type="checkbox" disabled checked />  Beweise Hgramm übergeben: [Session 20](../log/session-20.md)