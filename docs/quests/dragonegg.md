## Allgemeines

| | | | |
| - | - | - | - |
| __Auftraggeber__  | Keeper of Flames                                 | __Bekommen__  |  [Session 14](../log/session-14.md)  |
| __Ort__           | [Gracklstugh](../locations/gracklstugh.md)       | __Gelöst__    |  [Session-19](../log/session-19.md)  |

## Beschreibung

Die Grey Ghosts haben ein Drachenei gestohlen und die Keepers of the Flame wollen es zurückhaben.

## Reward

Gegenstände und Gold vom Drachenhort

## Tasks

- <input type="checkbox" disabled checked /> Mitglieder der  Grey Ghosts identifiziert
- <input type="checkbox" disabled checked /> Drachenei aus den Whorlstone Tunnels holen [Session-19](../log/session-19.md)
- <input type="checkbox" disabled checked /> Das Ei den Keepern of the Flame übergeben [Session-20](../log/session-20.md)