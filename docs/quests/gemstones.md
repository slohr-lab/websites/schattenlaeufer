## Allgemeines

| | | | |
| - | - | - | - |
| __Auftraggeber__  | [Werz](../npc/saltbaron.md)                      | __Bekommen__  |  [Session 13](../log/session-13.md)  |
| __Ort__           | [Gracklstugh](../locations/gracklstugh.md)       | __Gelöst__    |                                      |

## Beschreibung

Werz Saltbaron hat uns einen Beutel ungeschliffener Spellgems gegeben welchen wir nach Blingdenstone zu xx bringen sollen. 

## Belohnung

10 pp und einen geschliffenen Spellgem pro Charakter.

## Tasks

- <input type="checkbox" disabled checked />  [Jules](../pc/jules.md) Beutel mit Gemstones bekommen
- <input type="checkbox" disabled  />  Nach Blingenstone reisen
- <input type="checkbox" disabled  />  Beutel an [Kazook Pickshine](../npc/pickshine.md) übergeben