
[![DDB](/assets/images/dndbeyond.png){: align=right width=100}](https://www.dndbeyond.com/profile/FloDaFreak/characters/42522596)

## Informationen

<br>

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Bhalrik Dornenschlag                  | __Haare:__        | rot           |
|__Rasse:__         | Hügelzwerg                            | __Augen:__        | grün          |
|__Klasse:__        | Druide                                | __Größe:__        | 1,49 m        |
|__Statur:__        | kräftig                               | __Alter:__        | 53            |
|__Rolle:__         | Support, Explorer, Frontliner         | __Geschlecht:__   | männlich      |

## Aussehen
![Placeholder](/assets/images/characters/bhalrik.jpg){: align=right width=300}

Bhalrik ist ein gut 1.50 Meter großer Zwerg. Für Zwergen typisch breit gebaut. Er ist in einer Lederrüstung gekleidet und ist mit einer sehr einzigartige Keule bewaffnet. Die Keule ist ein Hohler Ast aus dem eine Geode zu wachsen scheint.

## Verhalten

Bhalrik ist ruhig und besonnen, er überlegt sich jedes Wort was er sagt.

