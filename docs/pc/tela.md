[![DDB](/assets/images/dndbeyond.png){: align=right width=100}](https://www.dndbeyond.com/profile/Paxarion/characters/43022121)

## Informationen

<br>

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Tela                                  | __Haare:__        | lila          |
|__Rasse:__         | Tiefliing                             | __Augen:__        | gold          |
|__Klasse:__        | Rogue                                 | __Größe:__        | 1,72 m        |
|__Statur:__        | schlank                               | __Alter:__        | 18            |
|__Rolle:__         | Diebstahl, Schleichen                 | __Geschlecht:__   | weiblich      |


## Aussehen

![Placeholder](/assets/images/characters/tela.jpg){: align=right width=300}

Tela ist eine ansehbare Tiefling Dame. Sie trägt eine Lederrüstung und ist sowohl mit zwei Kurzschwertern als auch mit einer Handarmbrust bewaffnet. Sie bewegt sich geschmeidig und hat wachsame Augen.

## Verhalten

Als wirklich schön würde man sie lediglich bezeichnen wenn sie schläft. Sobald sie wach ist überstrahlt ihre Arroganz und Kälte die optischen Vorzüge bei Weitem. Sie spricht ohne nachzudenken und achtet auch keineswegs auf ihre Ausdrucksweise. Beleidigungen spricht sie häufig hinter den Rücken der Personen in Infernal aus - sicher das nur sie und [Tessaia](tessaia.md) dies versteht.

## Anmerkungen

### Session 01
Tela und [Tessaia](tessaia.md) scheinen eng befreundet zu sein und sprechen auch immer zu zwei in Infernal miteinander. Dies machen sie um sich zu beraten oder einfach nur über ihre anderen Gruppenmitglieder herzuziehen. Nicht wissend das [Neldor](neldor.md) sie verstehen kann.

### Session 5

Geht ihren eigenen Weg ohne Rücksicht auf die Sicherheit der Gruppe. Tut alles in ihrer Macht um nicht Teil der Gruppe zu sein. Kurz nachdem sie den Sporennetzwerk beigetreten ist wollte sie sofort wieder raus.