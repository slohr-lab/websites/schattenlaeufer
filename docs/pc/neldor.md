
[![DDB](/assets/images/dndbeyond.png){: align=right width=100}](https://www.dndbeyond.com/profile/slohr/characters/42500679)

## Informationen

<br>

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Neldor Vallar                         | __Haare:__        | zottelig, schwarz|
|__Rasse:__         | Halb-Elf                              | __Augen:__        | blau, mit goldenen Sprenklern|
|__Klasse:__        | Warlock (Hexblade)                    | __Größe:__        | 1,80 m        |
|__Statur:__        | hager                                 | __Alter:__        | 180           |
|__Rolle:__         | Frontliner, Beatdown, Face            | __Geschlecht:__   | männlich      |

## Aussehen

![Neldor](/assets/images/characters/neldor.png)

Neldor ist ein sehr dürrer, ja fast schon abgemagert aussehender, Halb-Elf. Seine zotteligen schwarzen Haare wie auch Bart überdecken seine großen Ohren teilweise aber schaffen es nicht ganz. Er hat eine recht blasse, ja schon fast leicht bläulich wirkende Haut. Sein gesamter Oberkörper wie auch Beine sind mit verschiedensten Tätowierungen überdeckt. 

<!--

## Notizen für nächste Sitzung

## Gracklstugh

### Potions:
  - __various__:
    - [Potion of Healing](https://www.dndbeyond.com/magic-items/potion-of-healing)
  - __common__
    - [Potion of Climbing](https://www.dndbeyond.com/magic-items/potion-of-climbing)
  - __uncommon__:
    - [Oil of Slipperness](https://www.dndbeyond.com/magic-items/oil-of-slipperiness)
  - __rare__
    - [Potion of Gasseaous Form](https://www.dndbeyond.com/magic-items/potion-of-gaseous-form)
    - [Potion of Invulnerability](https://www.dndbeyond.com/magic-items/potion-of-invulnerability)
    - [Potion of Heroism](https://www.dndbeyond.com/magic-items/potion-of-heroism)
  - __very rare__
    - [Potion of Invisibility](https://www.dndbeyond.com/magic-items/potion-of-invisibility)

### Spells:
- __3rd Level__
  - [Phantom Steed](https://www.dndbeyond.com/spells/phantom-steed)
- __5th Level__
  - [Telepathic Bond](https://www.dndbeyond.com/spells/telepathic-bond)

### Components:
- sehr dünnes Blei-Gewebe für die Rüstung (damit ich unterhalb der Rüstung eine Handy Pouch machen kann)
- sehr dünnes Blei-Gewebe für die Bag of Holding
- Spell Component: 1st - Find Familiar
- Spell Component: 4th - Summon Aberration (a pickled tentacle and an eyeball in a platinum-inlaid vial worth at least 400 gp)
- Spell Component: 7th - Forcecage (ruby dust worth 1,500 gp)

### Gegenstände:
- __various__
  - awakend spellbook
  - spell gems
- __common__
  - [Chest of Preserving](https://www.dndbeyond.com/magic-items/chest-of-preserving)
- __uncommon__
  - [Driftglobe](https://www.dndbeyond.com/magic-items/driftglobe) 
  - [Winged Boots](https://www.dndbeyond.com/magic-items/winged-boots) 
  - [Mithral Half-Plate](https://www.dndbeyond.com/magic-items/mithral-armor)
  - [Boots of Elvenkind](https://www.dndbeyond.com/magic-items/boots-of-elvenkind)
  - [Piwafwi](https://www.dndbeyond.com/magic-items/piwafwi-cloak-of-elvenkind)
  - [Immovable Rod](https://www.dndbeyond.com/magic-items/immovable-rod)
  - [Gloves of Thievery](https://www.dndbeyond.com/magic-items/gloves-of-thievery)
  - [Ointment](https://www.dndbeyond.com/magic-items/keoghtoms-ointment)
  - [Decanter of Endless Water](https://www.dndbeyond.com/magic-items/decanter-of-endless-water)
  - [Wand of Magic Missles](https://www.dndbeyond.com/magic-items/wand-of-magic-missiles)
- __rare__
  - [Folding Boat](https://www.dndbeyond.com/magic-items/folding-boat)
  - [Mithral Half-Plate +1](https://www.dndbeyond.com/magic-items/mithral-half-plate-1)
  - [Ring of Protection](https://www.dndbeyond.com/magic-items/ring-of-protection)
  - [Ring of Spell Storing](https://www.dndbeyond.com/magic-items/ring-of-spell-storing)
-->

## Verhalten

Neldor ist ein Einzelgänger. Er zieht sich gerne zurück und sucht nicht aktiv den Kontakt mit anderen. Er ist impulsiv und wenn jemand nicht seiner Meinung ist kann er durchaus auch sehr gerade heraus sagen was ihm daran nicht gefällt. Er scheint eine Abneigung gegen [Tela](tela.md) und [Tessaia](tessaia.md) zu haben. 

## Eigenheiten

Er spricht Infernal und versteht daher jedes Wort was [Tela](tela.md) und [Tessaia](tessaia.md) so miteinander sprechen. 

## Personas

![Neldor](/assets/images/characters/thordan.jpg){: align=left width=300}

### Thordan Iceforge

Thordan Iceforge ist ein sehr großer Duergar Krieger. Neldor hat sich von [Hemeth](../npc/hemeth.md) inspirieren lassen und ist ein Bewacher von Karawanen im Unterreich.

