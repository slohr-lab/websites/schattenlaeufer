## Informationen

<br>

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Wela                              | __Status:__       | lebend        |
|__Rasse:__         | Fey (Firefox)                     | __Geschlecht:__   | weiblich      |

## Aussehen

![Placeholder](/assets/images/characters/wela.png)