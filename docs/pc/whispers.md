[![DDB](/assets/images/dndbeyond.png){: align=right width=100}](https://www.dndbeyond.com/monsters/tressym)

## Informationen

<br>

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Whispers                              | __Status:__       | lebend        |
|__Rasse:__         | Fey (Tressym)                         | __Geschlecht:__   | männlich      |
## Aussehen

![Placeholder](/assets/images/characters/whispers.png)

Whsipers der Familiar von Neldor welcher mit Hilfe des __Find Familiar__ Zaubers erschaffen wird. Die natürliche Form ist die eines Tressyms mit dunkelbaluer an schwarz grenzender Fellfarbe mit ein paar silberenen Sprenklern drinnen welche an den Nachthimmel erinnern. 

## Verhalten

Am liebsten liegt Whispers in der Kapuze von Neldor. 

## Eigenheiten

