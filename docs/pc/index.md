## Sprachen

### Spieler

| Charakter             | Common                             | Undercommon                       | Zwergisch                           | Elfisch                            | Infernal                           |
| ----------------------|:----------------------------------:|:---------------------------------:|:-----------------------------------:|:----------------------------------:|:----------------------------------:|
| [Bhalrik](bhalrik.md) | :fontawesome-solid-check-circle:   |                                   | :fontawesome-solid-check-circle:    | :fontawesome-solid-check-circle:   |                                    |
| [Neldor](neldor.md)   | :fontawesome-solid-check-circle:   |:fontawesome-solid-check-circle:   |                                     | :fontawesome-solid-check-circle:   | :fontawesome-solid-check-circle:   |
| [Jules](jules.md)     | :fontawesome-solid-check-circle:   |                                   | :fontawesome-solid-check-circle:    |                                    |                                    |
| [Severin](severin.md) | :fontawesome-solid-check-circle:   |:fontawesome-solid-check-circle:   |                                     |                                    |                                    |

### Allierte

| Charakter                                 | Common                             | Undercommon                       | Zwergisch                           | Elfisch                            | Gnomish                            | Orcish           |
| ------------------------------------------|:----------------------------------:|:---------------------------------:|:-----------------------------------:|:----------------------------------:|:----------------------------------:|:----------------:|
| [Derendil](../npc/derendil.md)            |                                    |                                   |                                     | :fontawesome-solid-check-circle:   |                                    |                  |
| [Eldeth](../npc/eldeth.md)                | :fontawesome-solid-check-circle:   |                                   | :fontawesome-solid-check-circle:    |                                    |                                    |                  |
| [Sarith](../npc/sarith.md)                |                                    | :fontawesome-solid-check-circle:  |                                     | :fontawesome-solid-check-circle:   |                                    |                  |
| [Stool](../npc/stool.md)                  | :fontawesome-solid-check:          | :fontawesome-solid-check:         | :fontawesome-solid-check:           | :fontawesome-solid-check:          | :fontawesome-solid-check:          |:fontawesome-solid-check:|
| [Hemeth](../npc/hemeth.md)                | :fontawesome-regular-check-circle: | :fontawesome-solid-check-circle:  | :fontawesome-solid-check-circle:    |                                    |                                    |                  |


### Neutral

| Charakter                                 | Common                             | Undercommon                       | Zwergisch                           | Elfisch                            | Gnomish                            | Orcish           |
| ------------------------------------------|:----------------------------------:|:---------------------------------:|:-----------------------------------:|:----------------------------------:|:----------------------------------:|:----------------:|
| [Topsy & Turvy](../npc/topsy_turvy.md)    |                                    | :fontawesome-solid-check-circle:  |                                     |                                    | :fontawesome-solid-check-circle:   |                  |

### Feinde

| Charakter                                 | Common                             | Undercommon                       | Zwergisch                           | Elfisch                            | Gnomish                            | Orcish           |
| ------------------------------------------|:----------------------------------:|:---------------------------------:|:-----------------------------------:|:----------------------------------:|:----------------------------------:|:----------------:|
| [Buppido](../npc/buppido.md)              |                                    | :fontawesome-solid-check-circle:  | :fontawesome-solid-check-circle:    |                                    |                                    |                  |
