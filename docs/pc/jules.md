
[![DDB](/assets/images/dndbeyond.png){: align=right width=100}](https://www.dndbeyond.com/profile/Neickel/characters/42926214)

## Informationen

<br>

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Jules                                 | __Haare:__        | kurz, weiß-gold|
|__Rasse:__         | Mensch                                | __Augen:__        | grün          |
|__Klasse:__        | Paladin                               | __Größe:__        | 1,88 m        |
|__Statur:__        | kräftig                               | __Alter:__        | 24            |
|__Rolle:__         | Frontliner, Beatdown, Support         | __Geschlecht:__   | weiblich      |



## Aussehen

![Jules](/assets/images/characters/jules.jfif){: align=right width=300}

Jules ist eine sehr große und muskulöse Menschen Frau. Sie hat kurze weiße Haare und ist mit einem Kettenhemd wie auch einem Schild gerüstet. Als Waffe führt sie ein Langschwert.

## Verhalten

Jules ist eine ruhige Person die Streit lieber aus den Weg geht. Man muss sie schon ziemlich auf die Palme bringen bevor sie Handgreiflich wird. In der Regel kann man das erreichen wenn man jemanden, den sie als schützenswert erachtet, angreift oder hänselt; alternativ kann man natürlich auch ihr gekochte Essen beleidigen oder mit Körperflüssigkeiten "aufwerten".

## Eigenheiten

Sie ist eine sehr gute Köchin und hat Höhenangst.