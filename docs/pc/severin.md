[![DDB](/assets/images/dndbeyond.png){: align=right width=100}](https://www.dndbeyond.com/profile/MacMallow/characters/42943487)

## Informationen

<br>

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Severin "Sev"                         | __Haare:__        | braun         |
|__Rasse:__         | Mensch                                | __Augen:__        | braun         |
|__Klasse:__        | Ranger                                | __Größe:__        | 1,72 m        |
|__Statur:__        | hager                                 | __Alter:__        | 17            |
|__Rolle:__         | Explorer, Beatdown                    | __Geschlecht:__   | männlich      |

## Aussehen

![Severin](/assets/images/characters/severin.jpg){: align=right width=300}


Severin ist ein hagerer kleiner junger männlicher Mensch. Er ist ein einer Lederrüstung gekleidet und hat einen Langbogen als bevorzugte Waffe geschultert.

## Anmerkungen

### Session 01

Severin unterwirft sich gerne und versucht Streit wo es geht zu vermeiden.
Er scheint ein besonderes Ärgernis mit Hängebrücken zu haben, da er sie immer zerstören möchte.

### Session 06

Irgendwie scheinen die anderen Gruppenmitglieder und Wesen die wir begegnen Severin stellenweise einfach nicht wahrzunehmen, auch wenn er direkt vor ihnen sitzt. Mir wäre dies nicht aufgefallen. Er ist immer ganz klar und eindeutig zu sehen.

Vermutlich hat dies damit zu tun das auch meine Augen sich besser auf die Dunkelheit angepasst haben und ich nun auch alles in Farben und Helligkeit sehe - so wie ich es von der Oberfläche gewohnt war.