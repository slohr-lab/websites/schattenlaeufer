[![DDB](/assets/images/dndbeyond.png){: align=right width=100}](https://www.dndbeyond.com/profile/Paxarion/characters/43022121)

## Informationen

<br>

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Tessaia                               | __Haare:__        | rot           |
|__Rasse:__         | Tiefling                              | __Augen:__        | gold-gelb     |
|__Klasse:__        | Bard                                  | __Größe:__        | 1,65 m        |
|__Statur:__        | schlank                               | __Alter:__        | 23            |
|__Rolle:__         | Unterstüzung                          | __Geschlecht:__   | weiblich      |

## Aussehen

![Placeholder](/assets/images/characters/tessaia.jpg){: align=right width=300}

Tessaia ist ein ebenso attraktiver wie auch eloquente Tiefling Dame. Sie trägt eine Lederrüstung und eine Vielzahl verschiedener Musikinstrumente. Als Waffe führt sie eine Handarmbrust als auch einen Dolch. 

## Verhalten

Tessaia ist es gewohnt Befehle zu erteilen und augenscheinlich noch nicht viel mit Leuten zu tun gehabt welche  ihren Befehlen nicht sofort nachkommen. Sie ist aufbrausend und kann durchaus eingeschnappt sein. Sie scheint wie eine große Schwester oder gar Mutter für [Tela](tela.md) zu sein. Sie nimmt [Tela](tela.md) in Schutz und kann sie auch runterholen wenn sich [Tela](tela.md) mal wieder in eine Situation verrannt hat.

## Eigenheiten

[Tela](tela.md) und [Tessaia](tessaia.md) scheinen eng befreundet zu sein und sprechen auch immer zu zwei in Infernal miteinander. Dies machen sie um sich zu beraten oder einfach nur über ihre anderen Gruppenmitglieder herzuziehen. Nicht wissend das [Neldor](neldor.md) sie verstehen kann.