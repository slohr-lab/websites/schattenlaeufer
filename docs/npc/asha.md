![Asha](/assets/images/characters/asha.jpg){: align=right width=300}

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Asha                                    | __Status:__       | lebend              |
|__Rasse:__         | Drow                                    | __Geschlecht:__   | weiblich            |

## Beschreibung

Asha ist eine junge aufstrebende und ergeizige Priesterin im Dienste von Lolth. Untergebene von [Ilvara](ilvara.md) in [Velkynvelve](../locations/velkynvelve.md). 

## Persönliche Notizen
