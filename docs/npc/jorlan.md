![Jorlan](/assets/images/characters/jorlan.jpg){: align=right width=300}

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Jorlan                                | __Status:__       | lebend           |
|__Rasse:__         | Drow                                  | __Geschlecht:__   | männlich         |

## Beschreibung

Jorlan war ehemaliger Anführer der Wachen von [Velkynvelve](../locations/velkynvelve.md) und ehemaliger Liebhaber von [Ilvara](ilvara.md). Er wurde in beiden abgelöst [Shoor](shoor.md) abgelöst. Dies geschah nachdem er schwer verwundet wurde und schwere Narben im Gesichte und einen verkrüppelten Arm hinnehmen musste. 

## Persönliche Notizen
