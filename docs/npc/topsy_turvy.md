## Informationen

### Turvy

![Turvy](/assets/images/characters/turvy.jpg){: align=right width=300}

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Turvy                                 | __Status:__       | lebend          |
|__Rasse:__         | Svirfneblin                           | __Geschlecht:__   | männlich        |


### Topsy

![Topsy](/assets/images/characters/topsy.jpg){: align=left width=300}

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Topsy                                 | __Status:__       | lebend          |
|__Rasse:__         | Svirfneblin                           | __Geschlecht:__   | weiblich        |


## Beschreibung

Die Svirfneblin Zwillinge sind ein putziges Paar. Sie scheinen noch recht jung zu sein und sind beide sehr geschickt. Beide kommen auch von [Blingdenstone](../locations/blingdenstone.md).

Topsy murmelt nur unverständliches Zeug allerdings übersetzt seine Schwester Turvy für seinen Bruder.

## Persönliche Notizen
