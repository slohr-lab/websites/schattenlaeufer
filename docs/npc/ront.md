![Ront](/assets/images/characters/ront.jfif){: align=right width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Ront                                 | __Status:__       | verstorben       |
|__Rasse:__         | Ork                                  | __Geschlecht:__   | männlich         |

## Beschreibung

Ront ist ein großer muskulöser Ork der ein reiner Bully ist. Er hänselt und translatiert die Leute wo er nur kann. Er hält sich für etwas besseres und für den natürlichen Anführer der Gruppe. Er ist ein Mitgefanger der Drow in [Velkynvelve](../locations/velkynvelve.md) und ist mit der Gruppe geflohen.

## Persönliche Notiz

### Session 01

Ront muss man näher im Auge behalten, er ist ein Unruhestifter und falls er für die Gruppe ein Risiko wird muss man sich um ihn kümmern.

### Session 02

Ront sieht sich als den natürlichen Anführer der Gruppe und musste mehrmals hier eingebremst werden, da er nur ein Mitläufer ist. Allerdings muss ich ihm durchaus zugestehen das er deutlich stärker ist als ich angenommen hatte. Das er dumm, in eigenen worten wohl eher "furchtlos", ist war mir klar aber ich habe unterschätzt wie stark er ist. Das er im Alleingang eine Riesenspinne mit zwei Schlägen seiner Pilzkeule vernichtet hat ist durchaus beeindruckend.

### Session 05

Ront wurde von [Buppido](buppido.md) ermordet während er vor Angst erstarrt war durch die Ankunft von Demogorgon