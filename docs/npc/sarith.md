![Sarith](/assets/images/characters/sarith.jfif){: align=right width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Sarith Kiekarit                       | __Status:__       | verstorben           |
|__Rasse:__         | Drow                                  | __Geschlecht:__   | männlich         |

## Beschreibung

Ein schweigsamer Drow welcher ebenfalls als Sklave verkauft werden sollte. Er betrachtet sich nach wie vor auch allen anderen Gefangenen als überlegen an zeigt es aber nicht direkt. Er fügt sich der Gruppe und widerspricht nicht. Er scheint froh zu sein seinen Peinigern entkommen zu sein. Er ist ein Mitgefanger der Drow in [Velkynvelve](../locations/velkynvelve.md) und ist mit der Gruppe geflohen.

## Persönliche Notizen

###  Session 01

Als Drow könnte er eine nützliche Informationsquelle sein, da er wohl die Umgebung und auch das Verhalten der Drow kennen sollte.

### Session 02

Sarith bewies das er eine gute Kenntnis der Gegend hat und konnte [Neldor](../pc/neldor.md) gut helfen eine Karte der Umgebung anzufertigen, welche das Navigieren viel einfacher macht.

### Session 03

Sarith ist des Mordes nicht abgeneigt und daher wohl keine Verwunderung das er des Mordes wegen hingerichtet werden soll. Keinem Drow sei zu trauen und alle müssten hingerichtet werden welche uns gesehen haben.