![Malgorn](/assets/images/characters/malgorn.jpg){: align=right width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Malgron Ironreach                     | __Status:__       | lebend        |
|__Rasse:__         | Duergar                               | __Geschlecht:__   | männlich      |

## Beschreibung

Ein sehr breitschultriger Duergar der in einer schweren Plattenrüstung gehüllt ist und einen großen zweihändigen Hammer schwingt. Er ist ein Warlord der Duergar und ist beheimatet in [Gracklstugh](../locations/gracklstugh.md).

## Anmerkungen

### Session 08

Wir haben Malgorn in einem versunkenen Tempel getroffen. Hier haben wir einen Deal geschlossen dass die Gruppe 1/3 des Fundes bekommt was immer dieser Tempel beinhaltet wenn sie ihnen dabei helfen einen Troll zu besiegen.

Malgorn hat Neldor in der Persona von __Thordan Iceforge__ kennengelernt.
