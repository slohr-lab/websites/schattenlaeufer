![Shoor](/assets/images/characters/shoor.jpg){: align=right width=300}

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Shoor                                 | __Status:__       | lebend           |
|__Rasse:__         | Drow                                  | __Geschlecht:__   | männlich         |
## Beschreibung

Shoor ist Anführer der Wachen und Liebhaber von [Ilvara](ilvara.md) in [Velkynvelve](../locations/velkynvelve.md). Er ist der Nachfolger von [Jorlan](jorlan.md). 

## Persönliche Notizen
