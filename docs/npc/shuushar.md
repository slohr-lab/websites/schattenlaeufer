![Shuushar](/assets/images/characters/shuushar.jfif){: align=right width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Shuushar, The Awakened                | __Status:__       | verstorben       |
|__Rasse:__         | Kuo-Toa                               | __Geschlecht:__   | männlich         |

## Beschreibung

Ein Kuo-Toa welcher sich selber als Erleuchtet bezeichnet da er, im Gegensatz zu vielen anderen seiner Rasse, geistig klar ist. Er kommt aus [Sloobludop](../locations/sloobludop.md) und möchte dort auch wieder zurück. Er ist ein Mitgefanger der Drow in [Velkynvelve](../locations/velkynvelve.md) und ist mit der Gruppe geflohen.

## Persönliche Notizen

### Session 03

Shuushar ist Pazifist der sich aus allen Kämpfen heraushält.

### Session 05

Shuushar verlies die Gruppe in [Sloodublop](../locations/sloobludop.md) noch bevor der Demogorgon angegriffen hat.