![Placeholder](/assets/images/characters/stool.png){: align=right }

## Informationen

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Stool                         | __Status:__       | lebend           |
|__Rasse:__         | Myconid                       | __Geschlecht:__   | unbekannt        |

## Beschreibung

Stool ist ein mykonidischer Spross. Der Hocker ist einsam und verängstigt und möchte nur in seine Heimat im Nimmerlicht-Hain zurückkehren. Wenn sich die Charaktere mit ihm anfreunden, bietet er ihnen gerne an, sie in seine Heimat zu führen und verspricht ihnen Zuflucht bei seinem Volk.

Stool benutzt Rapportsporen, um eine telepathische Kommunikation mit anderen Lebewesen herzustellen.

Er ist ein Mitgefanger der Drow in [Velkynvelve](../locations/velkynvelve.md) und ist mit der Gruppe geflohen.

## Persönliche Notizen

### Session 03
Stool stellt eine Telepathische Verbindung mit folgenden Personen her:

- [Jules](../pc/jules.md)
- [Severin](../pc/severin.md)
- [Tessaia](../pc/tessaia.md)
- [Neldor](../pc/neldor.md)
- [Bhalrik](../pc/bhalrik.md)
- [Eldeth](eldeth.md)

## Session 24

Stool blieb im Neverlight Grove. Da dieser jetzt auch sicher war (zumindest soweit sicher wie man im Underdark sein kann) verabschiedete er sich von der Gruppe als sie den Neverlight Grove verliesen in Richtung Blingendenstone.