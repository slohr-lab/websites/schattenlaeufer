## Informationen

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Dorun                                 | __Status:__       | lebend        |
|__Rasse:__         | Steinriese                            | __Geschlecht:__   | männlich      |

## Beschreibung

Dorun ist der Gehilfe von [Hgraam](hgraam.md) in [Gracklstugh](../locations/gracklstugh.md).