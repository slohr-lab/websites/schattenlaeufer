![Derendil](/assets/images/characters/derendil.jfif){: align=right width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Derendil                                   | __Status:__       | lebend           |
|__Rasse:__         | Quagoth                                    | __Geschlecht:__   | männlich         |

## Beschreibung

Derendil ist ein Elfenprinz welcher von einen mächtigen Magier in die Gestalt eines Quaggoth verwandelt wurde. Er spricht lediglich in Elfisch. Er ist ein Mitgefanger der Drow in [Velkynvelve](../locations/velkynvelve.md) und ist mit der Gruppe geflohen.

## Persönliche Notizen

Ich kann noch nicht sagen warum, aber ich glaube ihm nicht. Ich bin mir nicht sicher in wie weit er einfach nur verrückt ist oder eine glatte Lüge. 