![Eldeth](/assets/images/characters/eldeth.jpg){: align=right width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Eldeth                                | __Status:__       | lebend        |
|__Rasse:__         | Schildzwerg                           | __Geschlecht:__   | weiblich      |

## Beschreibung

Eine Schildzwergin aus [Gauntlygrym](../locations/gauntlgrym.md). Sie ist ausgerüstet mit einer Brustplatte, Schild und Streithammer. Sie ist eine geübte Schmiedin. Er ist ein Mitgefanger der Drow in [Velkynvelve](../locations/velkynvelve.md) und ist mit der Gruppe geflohen.

## Persönliche Notizen

Ich vertraue ihr soweit das sie wirklich daherkommt wo sie sagt und das ihre Absichten gut sind.