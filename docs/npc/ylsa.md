## Informationen

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Ylsa                                 | __Status:__       | lebend        |
|__Rasse:__         | Duergar                              | __Geschlecht:__   | weiblich      |

## Beschreibung

Sie ist im Rat von [Gracklstugh](../locations/gracklstugh.md) und verantwortlich für den Handel und Reiserouten.

Sie verspricht uns das sie uns - für einen kleinen Gefallen - sicher aus dem Underdark bringen kann. Die Route würde von [Gracklstugh](../locations/gracklstugh.md) über [Neverlight Grove](../locations/neverlight_grove.md) nach [Blingdenstone](../locations/blingdenstone.md) führen und von dort wäre die sichereste Route an die Oberfläche.