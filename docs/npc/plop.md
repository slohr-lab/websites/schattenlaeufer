## Informationen

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Plop                                  | __Status:__       | lebend        |
|__Rasse:__         | Reisenechse                           | __Geschlecht:__   | weiblich      |


## Beschreibung

Plop wurde von [Neldor](../pc/neldor.md) von einigen Dunkelelfen Händlern abgenommen. Er überredete sie, in Verkleidung eines adeligen Drows, ihm Ausrütung und ein Reittier für seine Mission zu überlassen [siehe Session 03](../log/session-03.md).  Plop ist eine große Reitechse, welche gerne größere Spinnen verspeist. 

![Plop](/assets/images/characters/plop.jpg)
