![Picture](../assets/images/characters/fargas.jpg){: align=left width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Fargas Rumblefoot                                | __Status:__       | lebend        |
|__Rasse:__         | lightfood halfling                               | __Geschlecht:__   | männlich      |

## Beschreibung

## Session 25

Fargas wurde im Silken Path gefunden. Dort wurde er von Riesenspinnen gefangen und von uns befreit.