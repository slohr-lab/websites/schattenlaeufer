## Informationen

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Werz Saltbaron                        | __Status:__       | lebend        |
|__Rasse:__         | Duergar                               | __Geschlecht:__   | männlich      |


Er hat uns einen Beutel mit ungeschliffenen Spellgems für [Kazook Pickshine](pickshine.md) mitgegeben. Noch ausstehende Bezahlung 1 Spellgem / Person.