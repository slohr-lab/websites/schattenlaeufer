![Ilvara](/assets/images/characters/ilvara.jpg){: align=right width=300}

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Ilvara                                  | __Status:__       | verstorben          |
|__Rasse:__         | Drow                                    | __Geschlecht:__   | weiblich            |

## Beschreibung

Hohepriesterin von Lolth und Anführerin in [Velkynvelve](../locations/velkynvelve.md). 

## Persönliche Notizen
