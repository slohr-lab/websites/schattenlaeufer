![Placeholder](/assets/images/characters/buppido.jpg){: align=left width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Buppido                                  | __Status:__       | tot          |
|__Rasse:__         | Derro                                    | __Geschlecht:__   | männlich            |

## Beschreibung

Buppido, ein männlicher Derro, ist überraschend gesellig und gesprächig, zeigt einen scharfen Verstand und eine entwaffnende Art. Er ist ausgerüstet mit einer Lederrüstung, mehreren Dolchen und einer Handarmbrust. Er ist ein Mitgefanger der Drow in [Velkynvelve](../locations/velkynvelve.md) und ist mit der Gruppe geflohen.

## Persönliche Notizen

### Session 02

Neldor war etwas überrascht zu sehen das Buppido die Halskette mit humanoiden Fingerknochen an sich genommen hat.

### Session 03

Buppido ging auf [Jules](../pc/jules.md) zu als sie verängst war, da sie [Tela](../pc/tela.md) als auch [Tessaia](../pc/tessaia.md) im Stich gelassen haben. Des Weiteren hat er sie gegen die anstürmenden Spinnen verteidigt zusammen mit [Derendil](derendil.md) und [Eldeth](eldeth.md).

### Session 05

Buppido wurde von [Jules](../pc/jules.md) gesehen wie er [Ront](ront.md) umgebracht hatte. Dies ist passier da ihr durch den Anblick des [Demogorgons](demogorgon.md) der Wahnvorstellung erlegen ist seinen Trieb  sofort nachzukommen.

## Session 16
Buppido wurde in den Whorlstone Tunneln in einer Höhle angetroffen. Er griff direkt an, hatte allerdings keine Change gegen die Übermaht welche sich ihm entgegenstellte.