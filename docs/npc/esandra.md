![Picture](../assets/images/characters/esandra.png){: align=left width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Esandara                                | __Status:__       | lebend        |
|__Rasse:__         | Hag                                     | __Geschlecht:__   | weiblich      |

## Beschreibung

Nimmt die Gestalt einer jungen Elfin an.

## Persönliche Notizen

### Session 10

Wir haben sie zusammen mit ihrer Schwester [Eliana](eliana.md) in ihrer Höhle gefunden und herausgefunden das die Elfe nur eine Illusion ist und in Wahrheit sie eine Hag ist. Sie und ihre Schwester konnten die Gruppe aber überzeugen das sie lediglich in Frieden leben wollen.