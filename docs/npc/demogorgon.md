![Demogorgon](/assets/images/characters/demogorgon.png){: align=right width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Demogorgon                            | __Status:__       | lebend            |
|__Rasse:__         | Dämonenprinz                          | __Geschlecht:__   | männlich          |
|__Alias:__         | Leemooggoogoon                        |                   |                   |

## Beschreibung

Der an die 9 Meter große Dämon ist eine Mischung aus großen Affen und Schlange. Er wird durch Gewalt und Blutvergießen angelockt.

## Persönliche Notizen

###  Session 05

Wir haben mit erlebt wie der Demogorgon das Dorf [Sluubludop](../locations/sloobludop.md) zerstört hat. 

![Demogorgon](/assets/images/characters/demogorgon_darklake.png)