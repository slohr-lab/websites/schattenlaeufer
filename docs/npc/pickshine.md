## Informationen

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Kazook Pickshine                      | __Status:__       | lebend        |
|__Rasse:__         | Tiefengnom                            | __Geschlecht:__   | unbekannt     |


Zielperson der wir den Beutel mit ungeschliffenen Spellgems übergeben sollen, welche wir von [Werz Saltbaron](saltbaron.md) bekommen haben. Noch ausstehende Bezahlung 1 Spellgem / Person.