![Picture](../assets/images/characters/hemeth.png){: align=left width=300}

## Informationen

|  |  |  |  |
|-:|:-|-:|:-|
|__Name:__          | Hemeth Xornbane                       | __Status:__       | lebend        |
|__Rasse:__         | Duergar                               | __Geschlecht:__   | männlich      |

## Beschreibung

Hemeth ist ein Dunkelzwerg der sich der Gruppe in [Sloobludop](../locations/sloobludop.md) angeschlossen. Er sollte geopfert werden. Da er aus [Gracklstugh](../locations/gracklstugh.md) ist und dort wieder zurückmöchte schließt er sich, nach seiner Befreiung, der Gruppe an und ist ihr Führer über den Darklake.

## Persönliche Notizen

### Session 06

Hemeth begleitet beruflich Karawanen von und nach [Gracklstugh](../locations/gracklstugh.md). Er war mit einer Karawane nach [Sloodubop](../locations/sloobludop.md) unterwegs, als er gefangen genommen wurde. Er war der einzige überlebende dieser Karawane.