![Jimjar](/assets/images/characters/jimjar.jfif){: align=right width=300}

|  |  |  |  |
|-:|:-|-:|:-| 
|__Name:__          | Jimjar                                 | __Status:__       | verstorben       |
|__Rasse:__         | Tiefengnom                             | __Geschlecht:__   | männlich         |

## Beschreibung

Ein Svirfneblin aus [Blingdenstone](../locations/blingdenstone.md). Er ist ein Spieler und er wettet auf fast alles was man wetten kann! Er war ein Mitgefanger der Drow in [Velkynvelve](../locations/velkynvelve.md) und ist mit der Gruppe geflohen.

## Persönliche Notizen

Soweit ich das beurteilen kann ist er geschickt mit seinen Fingern.

## Wetten

| Person    | Betrag    | Ergebnis  | Wette                                         | Bezahlt   |
|-----------|-----------|-----------|-----------------------------------------------|-----------|
| Bhalrik   | 10 gp     | verloren  | Jimjar öffnet die Truhe in Ilvaras Gemächer   | nein      |
| Neldor | 40 gp | verloren | Neldor beim würfeln gewonnen | nein |

## Tod

Jimjar ist während der Nachruhe nach dem zweiten Reisetag im Schlaf ermordet worden [siehe Session 03](../log/session-03.md). Der Mord sah aus wie ein Ritualmord.