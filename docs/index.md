---
hide:
  - navigation # Hide navigation
  - toc        # Hide table of contents
---

# Die Gruppe

Eine bunt zusammengewürfelte Gruppe unterschiedlicher Rassen, Herkünfte und Motivationen sind allesamt in den Fängen der Dunkelelfen gelangt und in einen ihrer Außenposten zum Weitertransport nach [Menzoberranzan](locations/menzoberranzan.md) gesammelt. Hier lernen sich die Charaktere kennen und beginnen mit einem Ausbruch von ihren Peinigern ihre Reise ins Underdark.

![Image](assets/images/home.jpg)
