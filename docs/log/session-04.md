Nachdem [Neldor](../pc/neldor.md) sich zurückgezogen hatte wurde er unmittelbar von mehreren Kuo-Toa verfolgt und angegriffen. Er streckte die Angreifer mit seinem Schwert nieder, was durch die erhöhte Position auf Plop kein Problem darstellte. Aus den Augenwinkeln nahm er wahr das mehrere Kuo-Toa Buppido zu niederrangen und kurz darauf auch [Ront](../npc/ront.md) fiel. [Turvy und Topsy](../npc/topsy_turvy.md) zogen sich aus den Kampf zurück und flohen. Sie ignorierten alle Rufe und flohen vom Kampf und der Gruppe.

![Runde 4](../assets/images/log/session-04/round-04.png)

[Neldor](../pc/neldor.md) ritt neben dem größten Kuo-Toa der sich mit seinen großen Klauen [Ront](../npc/ront.md) zu Fall brachte. Er zingelte den Kuo-Toa zusammen mit [Bhalrik](../pc/bhalrik.md) ein während [Sev](../pc/severin.md) ihn mit einem gut platzierten Pfeil erlegte. Wie sich herausstellte war dies auch der letzte noch stehende Angreifer. Der Großteil der Angreifer wurde getötet insgesamt 3 Kuo-Toa wurden mit Drow Gift ausgeschaltet und wurde gefangen genommen für spätere Befragung am Leben gelassen.

Auch nach dem Kampf kamen [Turvy und Topsy](../npc/topsy_turvy.md) nicht wieder zur Party zurück. 

[Shuushar](../npc/shuushar.md) und [Neldor](../pc/neldor.md) verhörten die Gefangen und finden ein paar nützliche Informationen raus. 

- [Leemooggoogoon](../npc/demogorgon.md) hat die Sea-Mother als Gott vor ein paar Monate abgelöst.
- Ploopploopeen (alter Stammesführer) ist nicht mehr von Bedeutung, verehrt immer noch die Sea-Mother.
- Bloppblippodd (Tochter von Ploopploopeen) ist Hohepriesterin von [Leemooggoogoon](../npc/demogorgon.md).

Auch der pazifistische [Shuushar](../npc/shuushar.md) war überzeugt das die Gefangen nicht am Leben gelassen werden können da sie das vorankommen der Gruppe massive behindern würden und ggf. das gesamte Dorf gegen die Gruppe aufbringen würde. [Neldor](../pc/neldor.md) erkannte das die Gruppe nicht bereit war die notwendigen Schritte zu unternehmen und bat die Gruppe schon vorzugehen und er kommt nach. Die Gruppe schaute sich kurz in die Augen und ging voraus. [Neldor](../pc/neldor.md) wartete einigen Minuten bevor er sich um die Gefangenen kümmerte und dann auf [Plop](../npc/plop.md) demnächst mit der Gruppe aufschloss.

