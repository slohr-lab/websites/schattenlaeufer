## Opfergaben?

Die Gruppe machte sich, nach einer erholsamen Nachtruhe, auf Richtung [Sloobludop](../locations/sloobludop.md). [Neldor](../pc/neldor.md) fühlte sich erholt als sie sich auf den Weg machten. der Flusslauf erhellte die Umgebung das sie den Rest der Strecke in einer sehr hellen Umgebung gehen konnten, es dauerte eine Weile bis [Neldor](../pc/neldor.md) begriff das sich seine Augen wohl an die Dunkelheit gewöhnt hatten und er einfach deutlich besser in der Dunkelheit sehen konnte.

Nach einigen Stunden entdeckte die Gruppe einige Kuo-Toa welche ihnen entgegnen kamen. Wild fuchteln, aber mehr freundlich als gefährlich wirkend, kamen sie auf die Gruppe zu. Es stelle sich heraus das dies der Hohepriester Ploopploopeen (aka Ploop) war welcher um Hilfe bat seine Tochter Bloppblippodd (aka Blopp) auszuschalten - da die Verehrung von [Leemooggoogoon](../npc/demogorgon.md) ein zu gefährliches Maß annähme. Ein Plan wurde geschmiedet das die Gruppe als weiter Opfergaben sich ausgeben sollte für [Leemooggoogoon](../npc/demogorgon.md) und dann sollte die Gruppe Blopp ausschalten - als Belohnung sollten ihnen Schiffe, Proviant und Führer für die Überfahrt über den Darklake zur Verfügung gestellt werden.

Die Gruppe willigte ein und [Tela](../pc/tela.md) sowie [Severin](../pc/severin.md) fesselten die anderen Mitglieder so das sie jederzeit ihre Fesseln lösen konnten. Lediglich [Tela](../pc/tela.md) und [Neldor](../pc/neldor.md) blieben ungefesselt und sie verwandelten sich in Kuo-Toa und gaben sich als weitere Wachen aus kurz bevor sie die Siedlung erreichten.

## Sluudopblop

![Gate](../assets/images/log/session-05/gate.png){: align=left width=300}

[Sloobludop](../locations/sloobludop.md) befand sich am Osf-Ufer des Darklakes und aus der Entfernung sah es aus als ein riesiges Gewirr aus Algen und Seetang, das von phosphoreszierenden Flecken beleuchtet wurde. Aus diesem Haufen Seetang erstreckten sich vereinzelte Türme welche mit Seilen und kleinen Planken verbunden und wohl auch zusammengehalten wurden. Die Siedlung der Kuo-Toa war von vielen Fischnetzen umgeben was wohl als Zaun dienen sollte. Die Gruppe wurde ohne weitere Anstalten in die Siedlung gelassen was wohl der Begleitung von Ploop lag. 

Neldor führte [Plop](../npc/plop.md) an den Zügeln hinter sich her und sie begaben sich durch die Siedlung zu einem Altar. Dies schein der Altar der Seemutter zu sein. Gute 30 Kuo-Toa waren um den Altar versammelt und vier große muskulöse Kuo-Toa mit Peitschen in den Händen waren um den Schrein positioniert. 

Ploop marschierte los um seine Tochter zu holen und bedeutete der Gruppe hier zu warten. Er würde seiner Tochter die "Opfergaben" vorstellen damit sie einen Termin für das Opferritual ausmachen konnte. [Tela](../pc/tela.md) in Fischgestalt folgte ihm. [Neldor](../pc/neldor.md) machte einen weiteren Schrein aus mit einer weiteren Humanoiden Opfergabe. [Neldor](../pc/neldor.md) gab die Zügel von Plop weiter nach [Jules](../pc/jules.md) und begab sich in die Richtung. Der andere Schrein war mehrere dutzend Meter vom Schrein der Seemutter entfernt. Ein ähnliches Bild bot sich hier das mehrere Leute um den Schrein herumstandne und ihn anbeten während größere Kuo-Toa mit Peitschen am Schrein standen.

## [Hemeth](../npc/hemeth.md)

![Hemeth](../assets/images/characters/hemeth.png){: align=right width=300}

Wie es sich herausstelle war der Gefangene ein Tiefenzwerg names [Hemeth](../npc/hemeth.md). Er war sehr überrascht das ihn einer der Kuo-Toa ansprach, [Neldor](../pc/neldor.md) konnte ihm allerdings schnell klar machen das sie Abenteuer waren welche auf der Durchreise nach [Gracklstugh](../locations/gracklstugh.md). [Neldor](../pc/neldor.md) schnitt ihm unauffällig die Fesseln durch. 

Über die Telepathische Verbindung kamen mehrere Stimmen durch und wie es aussah wollte [Tela](../pc/tela.md) die Hohepriesterin sofort töten. Es wurde sich gegen den Plan ausgesprochen. [Neldor](../pc/neldor.md) erklärte das es gut ist wenn sie wüsste wo sich die Priesterin zur Ruhe begibt aber vorerst sollten wir keinen überstürzten Handlungen vornehmen. [Tela](../pc/tela.md) informierte die Gruppe das sie die Priesterin jetzt ausschalten würde und ignoriete das laute "NEIN" was [Neldor](../pc/neldor.md) in die Telepahtische Verbindung schrie. [Neldor](../pc/neldor.md) warf sein Schwert und Schild dem Zwerg hin und warnte ihm was gleich kommen sollte.


## Ermordung einer Priesterin

![Round 01](../assets/images/log/session-05/round-01.png)

Ein lauter Schrei ertönte aus südlicher Richtung und viele Kuo-Toa welche um den Schrein standen machen sich auf den Weg nach Süden auf. [Neldor](../pc/neldor.md) ergriff die Gelegenheit und packte ein paar der Edelsteine und Münzen ein während sich [Hemeth](../npc/hemeth.md) das Schild umschnallte und das Schwert in die Hand nahm. 

Ein weiterer Ruf brach durch die Stille. "Die Priesterin des Leeeemoogorgon ist nicht mehr! Lang lebe die Seemutter!" und mit diesen Worten brach ein wahrer Sturm los. Kuo-Toa griff Kuo-Toa an. [Neldor](../pc/neldor.md) deute [Hemeth](../npc/hemeth.md) mit ihm die Boote fertig zu machen während er Telepathisch verkündete: "Nach Norden. Am Dock sind genug Schiffe!"

Als [Neldor](../pc/neldor.md) und Hemth die Boote zu Wasser lassen wollten vernahm [Neldor](../pc/neldor.md) aus den Augenwinkeln wie ihm ein Kuo-Toa anspringen wollte. Leichte Panik stieg in ihm auf, als er bemerkte das er sein Schwert als auch sein Schild [Hemeth](../npc/hemeth.md) gegeben hatte. "Ich bin bei dir" erklang erneut in seinem Kopf und er spürte wie sich eine Schwarz-Blaue Energie um ihn herum sammelte und sich dann manifestierte in seiner Hand in Form eines zweihändigen Schwertes. Mit Hilfe des Schwertes gelang es ihm den angreifenden Kuo-Toa abzuwehren und stellte sich seinen nächsten Angreifern in den Weg.

![Round 03](../assets/images/log/session-05/round-03.png)

## [Leemooggoogoon](../npc/demogorgon.md)

Doch dann geschah es. Ein lautes Brüllen erfüllte die gesamte Höhle. [Neldor](../pc/neldor.md) blickte auf den Seh hinaus und konnte aus dem Wasser aufsteigend einen riesigen, nein zwei riesige Köpfe sehen welche von dicken Tentakeln gefolgt wurde. Sein Blut gefror in seinen Adern, den er erkannte sofort wen er vor sich hatte. [Demogorgon](../npc/demogorgon.md) - einem Dämonenprinz. Er war aus dem Darklake gestiegen und bewegte sich auf sie zu! Es begann sich eine Panik in ihm auszubreiten als er sich an den Griff seines Schwertes erinnerte und sein Schwert schien zu pulsieren. Er schüttelte die Panik ab und schaute sich konzentriert um bevor er telephatisch mit der Gruppe kommunizierte "Ein Dämonenprinz ist aufgetaucht und kommt auf uns zu! Geht zu den Westlichen Docks - Dort sich auch genug Boote für uns alle!". [Neldor](../pc/neldor.md) wusste das der [Demogorgon](../npc/demogorgon.md) durch das angerichtete Blutbad hervorgelockt wurde.

Hemeth klammerte sich an ihn und [Neldor](../pc/neldor.md) zog den Zwerg am Dock entlang nach Westen. Zum Glück kamen sie ohne Angriffe der Kuo-Toa aus da sie entweder vor Angst erstarten oder sich zu Boden warfen um ihren Gott anzubeten.

![Round 05](../assets/images/log/session-05/round-05.png)

Neldor nahm aus den Augenwinkeln wahr das der Rest der Gruppe seine Nachricht verstanden hatte und sich aufmachten Richtung der Docks im Westen. Er wartete bis alle Gefährten an ihm vorbeigelaufen waren und legte eine Kugel der Dunkelheit zwischen ihnen und [Demogorgon](../npc/demogorgon.md), welcher gerade an Land gekommen war und nun in der Stadt tobte.

Es dauerte noch wenige Sekunden bis fast alle der Gruppe hier waren. [Jules](../pc/jules.md) sah ziemlich geschockt aus und sagte gerade das sie mitansehen musste wie Buppido Ront kaltblütig ermordet hat. Somit waren sie vollzählig und mussten nicht mehr auf die beiden warten.

## Flucht ins Ungewisse

![Flucht](../assets/images/log/session-05/flucht.png)

Sie machten sich auf und begaben sich auf den Darklake um in Sicherheit zu gelangen.

Nachdem die Gruppe weit genug weg war rügte [Neldor](../pc/neldor.md) [Tela](../pc/tela.md) für ihr Verhalten. [Tela](../pc/tela.md) warf ihm vor die Gruppe mindestens so in Gefahr gebracht zu haben wie sie als er mit dem Drow-Händlern gesprochen hatte. [Tela](../pc/tela.md) bestand darauf aus dem Telephatischen Netzwerk herausgenommen zu werden. [Neldor](../pc/neldor.md) fragte sich in wie weit hier Vertrauen aufgebaut werden soll wenn jemand aktiv gegen die Gruppe vorgeht. Dunkle Zeiten scheinen zu kommen und alle Leute um [Neldor](../pc/neldor.md) herum verfallen in Wahnsinn - der einzige Lichtblick für ihn war die Stimme in seinem Kopf welche Schutz vor seinen eigenen Dämonen bot.

So begann die Reise über den Darklake.

![Map](../assets/images/log/session-05/map.png)