## Rockblight

Die Gruppe begab sich in die Rockblights.

### Alte Siedlung

Skelett gefunden was aus dem Bett gestürzt

- 6 x Gems 100 gp
- 1 Unsicherbarkeitstrank

Neldor packt das Skelett (Udhask’s Skeleton) in seine bag of holding (um sie in den Katakomben zur ruhe zu betten).

### Wasserfall

Beim Wasserfall gegen Gargoyle und einem Erdelementar gekämpft. das elementar hat einen Edelstein dabei.

Beute: 
- gelber Diamant: beschwört Erdelementar.

### Medusa

Medusa gefunden und von Severin fast im Alleingang getötet. Die Medusa wurde von ihm überrascht und mit Pfeilen durchlöchert bevor sie die Gruppe überhaupt erreichen konnte.

Sie finden wertvolle Kleidung aus Spinnenseide und Neldor nimmt als Beweis den kopf der Medusa mit.

### Obelisk

Ein weitere Obelisk (wie in den Whorlstone Tunnels in Gracklstugh) wurde gefunden.

### Drow Statuen

Es gab noch einen Raum mit mehreren versteinerten Dunkelelfen Statuten welche alle nach einander zum Leben erweckt wurden und die Gruppe angegriffen haben. Nach einigen zerstörten Statuen konnte Neldor ein Wesen aus der Erdebene hinter den Angriffen steckte. Neldor versucht das Wesen zu bannen und es gelang ihm.