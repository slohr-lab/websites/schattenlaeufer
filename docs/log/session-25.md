## wieder underdark

erlun ist missmuting weil zuviel wasser im fluss ist. das underdark ändert sich im moment häufig. seit gut über einem monat ändert sich das underdark häufiger.


- tag 1: reise ist ereignislos ; erluns laune wird immer schlechter ; guter fortschritt ; ereignislose nacht
- tag 2: erluns fluchen wird viel lauter ; silken path ;


## silken path

eine rießige höhle mit dicken neuen spinnennetzern welche sich über eine tiefe schlucht spannt. es wird alles auf die echsen umgeladen.

neldor teilt sich plop mit jules.

die gruppe entdeckt eine auf einem abgebrochenen stalagtiten, welcher in den spinnenseilen hängt. zwei goblins (spiderbait und jugjug) sind dort anzutreffen. 

die goblins bieten für 4 gp an sie durch die spinnennetze zu führen. die gruppe willigt ein und macht noch eine lange rast bevor sie aufbrechen.

### tag 3

die zwei goblins führen sie auf direktesten weg nach norden. über kleine abgründe und dünne seile. bis sie am ende des tages ein großes skelett eines drachens in den spinnenseilen verhäddert vorfinden.

im schädel der überreste soll sich ein monster befinden. whispers kann einen untoten beholder erkennen. 

der zombie beholder wird in einem kurzes scharmützel getötet und anschließend werden einige knochen inklusive 

## tag 4

mehrere spinnen greifen uns immwe rwieder an, allerdings sind diese kein problem und können aus der entfernung ohne probleme getötet werden.

## tag 5

wir bewegung uns auf den kürzesten aber wohl auch geführlichsten weg zum ende. viele spinnen sollen hier auf uns warten. viele große spinnen. wir bereiten uns auf einen heftigen kampf vor.

![Spider Attack](../assets/images/log/session-25/spider-fight-1.png)

mehrere dutzend riesenspinnen werden von den goblins hervorgelockt bis nach wenigen sekunden eine gigantische spinne erscheint. neldor geht in den direkt angeriff über und das bewegen auf den spinnenseile ist etwas schwierig da sie nicht viel platz lassen. 

der schutzauber den neldor gewirkt hat scheint nicht wirklich zu wirken da die spinnen wohl über tremorsense (an den seilen) oder Blindsicht verfügen da sie ihn durch das heavy obscurement durch sehen können.

Nach mehreren Sekunden tauchte eine gigantische spinne auf welche neldor angriff. Neldor, Jules und Derendil griffen die gigantische Spinne an während der rest sich um die dutzenden kleineren kümmerte.

![Spider Attack](../assets/images/log/session-25/spider-fight-2.png)

Obwohl die Spinnen Neldor spüren konnte konnte er sehr gut den Angriffen ausweichen und selber eine kritische treffer bei der gigantischen spinne anbringen.  

In dem Spinnenhöhlen konnte einige Leichen sowie ein noch lebendes Wesen. Es handelt sich dabei um einen Halbling mit dem Namen [Fargas Rumblefoot](../npc/fargas.md). Er war bewusstlos aber am Leben und wurde mit der Gruppe mitbekommen.

Als die Gruppe das andere Ende der Schlucht erreicht hat wurden die zwei Goblins mit dem vereinbarten Betrag bezahlt.