## Runde 09

![Runde-09](../assets/images/log/session-07/round-09.png)

Nachdem der Kampf mit den Duergarn beendet worden ist hörte [Neldor](../pc/neldor.md) [Jules](../pc/jules.md) im Sporenetzwerk rufen das Dunkelelfen aus dem Norden kommen. Die Dunkelelfen griffen schossen sofort mit ihren Armbrustbolzen los. [Neldor](../pc/neldor.md) rannte geduckt in der Höhle um sich hinter einem Stalagmaten zu verstecken während er einen Eldrich Blast in Richtung der Drows abfeuerte. Bevor [Neldor](../pc/neldor.md) sich in Deckung hechtete konnte er erkennen wie er den Drow getroffen hatte und dieser zu Boden ging.

Im Sporen Netzwerk kam die Nachricht das wir uns in der größeren Höhle verbarrikadiern sollten warum sich [Jules](../pc/jules.md) zurückzog. [Derendil](../npc/derendil.md) (der nicht im Netzwerk war) rannte nach vorne um die Dunkelelfen direkt in den engen Tunnel abzufangen wurde aber durch ihre vergifteten Klingen niedergestreckt.

## Runde 10 und 11

![Runde-10](../assets/images/log/session-07/round-10.png)

[Neldor](../pc/neldor.md) hörte [Bhalrik](../pc/bhalrik.md) rufen das sich [Ilvara](../npc/ilvara.md) unter den Drow befand und auf sie zukommt. Alle Angriffe sollen auf Sie konzentriert werden. Als [Neldor](../pc/neldor.md) den Kopf erhob sah er gerade noch wie [Ilvara](../npc/ilvara.md) bereits in Flammen stand und sich gerade ein Pfeil von [Severin](../pc/severin.md) in die Brust traf. [Ilvara](../npc/ilvara.md) schrie wirres Zeug und [Neldor](../pc/neldor.md) schoss ein weiteres Geschoss auf sie ab. 

[Tessaia](../pc/tessaia.md) sagte etwas zu [Tela](../pc/tela.md) was [Neldor](../pc/neldor.md) nicht verstand, aber beide machten sich auf den Weg zu dem Tunnel bei dem sich auch die Boote befanden. [Neldor](../pc/neldor.md) konnte nicht sehen was sie hier machten allerdings hörte er wenige Sekunden später von [Stool](../npc/stool.md) im Netzwerk das sich mehrere Drows auf den Weg nach unten befanden. 

[Tela](../pc/tela.md) und [Tessaia](../pc/tessaia.md) kämpften gegen die Drow die von der rechten Seite kamen und [Neldor](../pc/neldor.md) hörte nur die Schmerzenschrei von [Tela](../pc/tela.md) sowie einen Hilferuf von [Tessaia](../pc/tessaia.md).

## Runde 12

![Runde-12](../assets/images/log/session-07/round-12.png)

Es sah ziemlich düster aus. [Neldor](../pc/neldor.md) bemerkte wie sich [Hemeth](../npc/hemeth.md) unsichtbar machte während er bemerkte das er und [Severin](../pc/severin.md) den gleichen Gedanken hatten. Sie bewegten sich von der linken Seite wieder in die Stalagmaten in der Mitte der Höhle zu (wo sich Sareth befand) und schossen beiden noch einmal auf [Ilvara](../npc/ilvara.md). Beide trafen sehr gut. Ein weiterer Pfeil von [Severin](../pc/severin.md) traf in die Brust von [Ilvara](../npc/ilvara.md) während [Neldor](../pc/neldor.md) mit einem lauten Zornes Schrei seinen Eldritch Blast in das Gesicht von [Ilvara](../npc/ilvara.md) versenkte und nur noch einen verkohlten Schädel zurücklies. [Neldor](../pc/neldor.md) konnte aus den Augenwinkeln sehen wie [Bhalrik](../pc/bhalrik.md) zusammen mit [Jules](../pc/jules.md) und [Eldeth](../npc/eldeth.md) in den die restlichen Dunkelelfen zurückdrängte. 

Ein aus Flammen zu bestehender Luchs sprang mit [Bhalrik](../pc/bhalrik.md) in der Ecke auch rum und griff die Drows an.

## Runde 13

[Neldor](../pc/neldor.md) sah das [Tessaia](../pc/tessaia.md) gerade zu Boden ging und [Tela](../pc/tela.md) wurde bewusstlos von einem Drow weggezogen. [Neldor](../pc/neldor.md) nam sein das Schwert in die Hand und bewegte sich auf die Drow zu und während er sich hinbewegte schoss er einen weiteren Eldritch Blast auf die Drow ab. [Jules](../pc/jules.md) rannte auf [Tessaia](../pc/tessaia.md) zu. Unter den Dunkelelfen befanden sich [Shoor](../npc/shoor.md) und [Asha](../npc/asha.md). [Neldor](../pc/neldor.md) rief auf unterkommen - verschwindet und lasst unsere Gefährten hier.

## Runde 14

![Runde-14](../assets/images/log/session-07/round-14.png)

Die Drow zogen ihre Kurzschwerter und rammten sie in die bewusstlosen Körper von [Tela](../pc/tela.md) und [Tessaia](../pc/tessaia.md) während [Asha](../npc/asha.md) einen Zauber wirkte welcher in ihrem Umkreis fliegende Wesen erschuf welche alles angriffen abgesehen der Drows. [Neldor](../pc/neldor.md) konnte sehen wie sich auch mit ihren spitzen Krallen nach [Tela](../pc/tela.md) und [Tessaia](../pc/tessaia.md) einschlugen.

Es entbrannte ein Stellungskampf während [Bhalrik](../pc/bhalrik.md), [Jules](../pc/jules.md), [Hemeth](../npc/hemeth.md) und [Neldor](../pc/neldor.md) den Drow nachsetzten während sie sich langsam zurückzogen. Sie erschlugen noch zwei weitere Dunkelelfen bevor sich die Drows aus dem Staub machen konnten. Da die Gruppe sehr stark verletzt war brachen sie die Verfolgung ab. Während [Jules](../pc/jules.md) und Bhalrek sich um [Tela](../pc/tela.md) und [Tessaia](../pc/tessaia.md) kümmerten. Leider kam für sie alle Hilfe zu spät.

## Beerdigung

[Jules](../pc/jules.md) suchte in der Höhle einen schönen trockenen Platz und begann Steine zu sammeln. Stumm begannen ihr [Eldeth](../npc/eldeth.md) und [Bhalrik](../pc/bhalrik.md) zu helfen. Sie begruben die zwei unter den Steinen. [Neldor](../pc/neldor.md) legte eine Karte mit in das Grab von [Tela](../pc/tela.md) - da er das Gefühl hatte dies könnte ein wichtiger Gegenstand für Tela gewesen sein. Man überlegt kurz ob man eines der Musikinstrumente mit [Tessaia](../pc/tessaia.md) begraben sollte kam dann aber zum Schluss das man sie deutlich besser ehren würde wenn man die Instrumte weiter für sie spielen würde.

![Grab](../assets/images/log/session-07/grave.png)

## Beute

| Gegenstand | Anzahl   | Bekommen von                | Charakter bekommen                    |
|-------------------|---|-----------------------------|---------------------------------------|
| __7 gp__              | 1 | versunkenem Tempel          | Bhalrik, Neldor, Jules, Eldeth, Severin, Hemeth, Sarith   |
| Tentackle Rod     | 1 | [Ilvara](../npc/ilvara.md)  | [Bhalrik](../pc/bhalrik.md)           |
| Studded Leather   | 5 | Drow                        | [Plop](../npc/plop.md) / Boot         |
| Shortsword        | 5 | Drow                        | [Plop](../npc/plop.md) / Boot         |
| Hand-Crossbow     | 5 | Drow                        | [Plop](../npc/plop.md) / Boot         |
| Bolt (Posion)    | 80 | Drow                        | [Plop](../npc/plop.md) / Boot         |
| Shield            | 8 | Drow /Duergar               | [Plop](../npc/plop.md) / Boot         |
| Javelin           | 7 | Duergar                     | [Plop](../npc/plop.md) / Boot         |
| Warpike           | 7 | Duergar                     | [Plop](../npc/plop.md) / Boot         |
| Scale-Armor       | 7 | Duergar                     | [Plop](../npc/plop.md) / Boot         |
| Javelin           | 7 | Duergar                     | [Plop](../npc/plop.md) / Boot         |
| -  | - | -  | - |
| Studded Leather  | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Dagger           | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Crossbow, Hand   | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Bolt (Posion)    | 17 | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Bolt             | 20 | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Disguise Kit     | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Dulcimer         | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Flute            | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Shawm            | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Gemstone (10gp)  | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Rope, Silk   | 100 ft | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Weaver's Tools   | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Clothes, Costume | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| Backpack         | 1  | Tessaia                     | [Plop](../npc/plop.md) / Boot         |
| -  | - | -  | - |
| Studded Leather  | 1  | Tela                        | [Plop](../npc/plop.md) / Boot         |
| Crossbow, Hand   | 1  | Tela                        | [Plop](../npc/plop.md) / Boot         |
| Bolt (Posion)    | 9  | Tela                        | [Plop](../npc/plop.md) / Boot         |
| Rope, Silk   | 100 ft | Tela                        | [Plop](../npc/plop.md) / Boot         |
| Shortsword       | 2  | Tela                        | [Plop](../npc/plop.md) / Boot         |
| Ring (7gp)       | 1  | Tela                        | [Plop](../npc/plop.md) / Boot         |
| Crowbar          | 1  | Tela                        | [Plop](../npc/plop.md) / Boot         |
| Thieves' Tools   | 1  | Tela                        | [Plop](../npc/plop.md) / Boot         |
| Book             | 1  | Tela                        | [Plop](../npc/plop.md) / Boot         |