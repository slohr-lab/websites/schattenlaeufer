## leaving on a lizard back

Am Tag der Abreise begaben wir uns zum vereinbarten Treffpunkt außerhalb der Tore von Gracklstugh. Die Luft war stickig und ich war mehr als glücklich die Stadt hinter mich zu lassen. Neben mir schnaufte Plop und ich bemerkte wie sein Kopf sich langsam an meine Gürteltasche zu schaffen machte. Ein lächeln kam über meine Lippen. Die Echse hat sofort gemerkt wo ich die gerösteten Spinnenbeine, welche sie liebte, aufbewahrte. Mit einer sanften Bewegung schob ich ihren Kopf weg und tätschelte ihr auf den Hals während ich gedankenverloren in die Gruppe schaute.

Neben mir schritt Jules her welche immer wieder in die Richtung der Keeper of the Flames schaute. Ich konnte fühlen, das es ihr nicht schmeckte das wir das Drachenei den Keepern gegeben haben was eher früher als später das Leben von Themberchaud besiegeln würde. Fressen oder gefressen werden ist der Alltag hier im Underdark. Bisher hatten sie sehr viel Glück und alles schien ihnen gewogen zu sein. Ganz ehrlich, wieviele Sterbliche sind auf Augenhöhe mit einem Dämonenlord gekommen und konnten von der Begegnung berichten, ganz zu Schweigen von Demogorgon einem Dämonenprinzen und dem Herscher des Abyss.

Sie erreichten die Karawane wo die letzten Vorbereitungen für ihren Aufbruch liefen. ES liefen ein gutes dutzend Duergar umher und beluden zwei gepanzerte Wägen welche von mehreren Echsen gezogen wurden. Es befanden sich auch mehrere große Spinnen und Echsen in der Nähe angebunden. Ilsa war auch anwesend und führte eine Unterhaltung mit einem stark gepanzerten Duergar. Als wir näher treten wechselte sie von zwergisch auf undercommon.

"Gut, das ihr schon hier seit. Hier habe ich Erdun. Er ist der Karawanenmeister und ihm unterstehen die anderen Wachen. Erdun, das sind noch weitere Mitreisendene die mit euch über den Neverlight Grove nach Blingdenstone reisen."

![Erdun](../assets/images/log/session-20/erdun.png){: align=left width=400} 

Erdun gab ein ein kleines Schnaufen von sich. Er war wohl kein Mann großer Worte. Er frage knapp wer von uns Reiten will oder lieber auf den Wagen Platz nehmen möchte? Neldor schwieg, da ihm klar war, das er auf Plop reiten würde; Stool und Rumpacasp bevorzugten den Wagen, Derendil bevorzugte es zu gehen während die anderen alle sich Lizards als Reittiere aussuchten.

Nach gut einer halben Stunde brach die Karawane auf. Es wurde lediglich ein kurzes Nachtlager, primär um die Tiere zu schonen, eingerichtet ansonsten wurde den ganzen Tag die Reise vorgesetzt. Ich zeichnete die Route mit und fertigte ein paar Karte an um einen besseren Überblick von der Gegend zu bekommen. Auch Jules, Bhalrik und Severin machten sich nützlich. Die Wachen hielten auf ihren Spinnen eine Formation ein welche die zwei Wägen im Mittelpunkt hatte.  

Die Duergar waren nicht sonderlich gespräch und das war Neldor auch ganz Recht. Er setzte sich am Abend meist hin und verschönerte die Karten. Sicherheitshalber erschuf er jeden Abend dennoch die undurchdringliche Kugel in der sich neben ihm auch seine Gefährten begaben um dort die Nacht etwas sicherer zu verbringen. Whispers hielt die Nacht über Ausschau da sie den Tag meist in der Kapuze seines Umhangs schlief.

Die Träume wurden intensiver seitdem sie Gracklstugh verlassen haben. Er konnte selbst im Wachzustand ein Gefühl freudiger Erregung nicht leugnen. Konnte es wirklich wahr sein das er demnächst das Ende seiner Suche erreichte? Er machte sich Gedanken wie er Erdun überreden konnte einen Umweg zu nehmen oder musste er sich dann entfernen um endlich zu seinem Ziel zu kommen.

## Der Untergehende Tempel

![sunken temple](../assets/images/log/session-20/sunken-temple.jpg)

Am dritten Tag der Reise gab der Stein unter einem der Wagen ein und Bhalrik, Derendil als auch Sev fielen in ein Loch. Jules und ich kletterenten nach um ihnen zu helfen. Die Duergar fluchten und versuchten den Wagen - welcher das Loch mittlerweile fast komplett zugedeckt hatte - wieder herauszubekommen.

Wie es aussah sind wird durch die Decke eines alten Tempels gebrochen. Meinte das Schicksal es geht mit mir? Ich konnte regelrecht die Anwesenheit von Zoras spüren. Es war ein alter Tempel und der Boden feucht von etwas Wasser.

Wir begannen den Tempel zu erkunden. Severin schlich vor und warnte uns von einigen grauem schleim welcher versuchte ihn anzugreifen allerdings er sich dessen sehr schnell entsorgt hatte.

Neldor konnte eine Stimme in seinem Kopf hören. Er freute sich schon allerdings war es eine andere Stimme als die die er schon so oft in seinen Träumen gehört hatte. Wie sich herausstellte war der Ursprung der Stimme ein sehr außergewöhnliches Wesen. Es war ein gelatinous cube der sich als Glabbagool vorstellte.

Glabbagool kam näher und dann konnte ich etwas schimmern sehen in ihm. Wie es aussah steckte Zoras in Glabbagool drinnen. Ich ging schnellen Schrittes auf dem Cube zu und entriss der Kreatur das Schwert. Der Kubus warnte mich noch, allerdings hörte ich seine Worte nicht mehr, ich spürte erst viel später die verätzung durch die Säure an meinen Händen. 

Ich nam das Schwert an mich und und konnte mein Glück nicht fassen. Ich hatte es geschafft. Endlich. Nach sovielen Jahren der Suche war meine Suche nun zu Ende. Vor Freude lachened und weinen begannn lies ich mich an einer Wand nieder und betrachtete das Schwert in meinen Händen. Ich hatte schon zuvor eine Schattenversion des Schwertes erschaffen können aber das richtige in den Händen zu halten war ein ganz anderes Gefühl. 

Zoras pulsierte in meinen Händen und ich konnte regelrecht spüren wie sehr er sich ebenso freute nicht mehr allein zu sein.





- Erdun karawanen führer - vertrauter von Ilsa
- zwei wagen
  - einer mit nahrung + wasser
  - einer mit waffen + zwei duergar zivilisten (trudy, jergen)
- alle in der gruppe nehmen ein reittier 

## Reise

- erster tag ereignislos
- zweiter tag ereignislos

- dritter tag: 
  - schmale schlucht
  - einer nach dem anderen müssen wir wandern
  - der boden gibt nach und wir landen in einer höhle
  - finden eine höhlensystem in dem sich schleime befinden
  - auch ein sentient gelatinous cube (Glabbagool)
  - der ein artefakt in sich hat welches neldor seit jahrzehnten gesucht hatte
  - neldor nimmt sich die waffe an sich ohne auf die säure zu achten die seinen arm zersetzt. freudestrahlend nimmt er die waffe 
  - 112 sp, 41 gp, 3 goldene armreifen (je 25gp), drow-dagger+1, potion of greater healing, oil of sliperiness, torch mace
  - flucht durch eine öffnung in den der höhlendecke aus der wasser austritt
  - Glabbagool kommt mit an die Oberfläche
  - vier große grey ooze kommen ebenfalls aus dem wasser und die gruppe kämpft gegen sie.

- die weiteren Tage verlaufen ereignislos
