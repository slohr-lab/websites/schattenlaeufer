## Stromschnellen

Nach wenigen Stunden wurde das Wasser im Fluss leicht schneller. Die Gruppe war zuerst erleichtert da sie nicht mehr so stark rudern mussten es wurde schnell zu besorgnis und kurz darauf angst, da es der Sog immer schneller wurde während die Höhlen immer enger wurden bis es zum Schluss zu einem reißenden Fluss wurde.

Die zwei Boote taten ihr besten um in der Mitte des Flusses zu bleiben und nicht an den unebenen Wänden zu zuerschellen. Hier schien das Wasser deutlich höher zu sein als es bisher jemals war, da die Wände alle sehr grober Stein war der noch nicht durch das Wasser abgeglättet worden ist.

Neldor lies Whispers voraus spähen und es ergab sich eine Abzweigung des Wasser wo das Wasser deutlich ruhiger zu sein schien. Die Besatzung des Kielbootes war deutlich kräftiger und sie konnten ihr Boot deutlich besser in den ausgang steueren als das Katamaran. Da die Gruppe beide Boote bereits zusammengebunden hatte, bevor es zu dieser starken Störmung kam, bestand die Gefahr das beide Boote wieder aus dem Höhlenausgang hinausgezogen wurden. Neldor Schnitt ihr Beiboot ab und anschließend ruderten sie bis zu Erschöpfung allerdings konnten sich noch in die abzweigende Höhle retten.

Dabei wurde das Katamaran allerdings mehrmals stark gegen die Tunnelwände gestoßen und daran entlang gezerrt. Während der weiterfahrt war der linke Teil des Bootes beriets teilweise im Wasser sie schafften es allerdings mit dem verlagern des Gewichtes eine rießige Höhle zu erreichen in der sich ein ebenso rießiger Fungal Wald befand.

## Fungal Wald 

![fungal](../assets/images/log/session-10/fungalcave.jpg)

Die Boote wurden an das Ufer gezogen und beide Boote wurden auf Schäden untersucht. Das Katamaran hat es sehr schwer erwischt und die gesamte linke Tragfläche muss wohl ausgetauscht werden damit es überhaupt noch länger seetauglich ist allerdings ist das Kielboot auf der anderer Seite ist nicht nennenswert beschädigt.

Das große Glück ist das sie in vor einem rießigen Fungal-Wald sind dessen Ende nicht abzusehen ist mit über zehn Meter hohen Zurkhwood Pilzen. Severin, Jules, Bhalrik und Neldor machten sich zusammen mit Plop in den Wald um geeignetes Holz zu finden und zurückzubringen. Sie schlugen einen sehr großen und zwei etwas kleinere Zurkhwood Pilze sowie etwas Trillimac damit sie ihre Ledervorräter auffüllen können.

Da sie alle ziemlich erschöpft waren begannen sie sehr schnell die Abendruhe. Neldor bat Bhalrik um eine geiwsste Ledertasche und überreichte ihm Zeichnung für die spezifische Tasche die er sich vorstellte.

Die Wachen waren alle ziemlich ereignislos allerdings war die ganze Zeit ein Gewimmer zu hören und Whispers konnte eine Höhle ausfindig machen aus jener welcher die Geräusche kamen. Die Geräusche konnten eindeutig Katzenartig identifiziert werden. Es wurde während dem einzelnen Wachen beschlossen bis in den morgen zu warten bis alle ausgeruht sind um sich die Sache genauer anzuschauen.

### Schiffsbau

Ebenfalls wurde beschlossen das Katamaran nicht mehr zu reparieren sondern das Kielboot soweit zu vergrößern das alle beteiligten zuzüglich Plop auf dem einen Boot Platz finden. Alle abgesehen von Jules, Bhalrik, Severin und Neldor machten sich an die Arbeit, während diese sich um die Höhle kümmerte damit sie zumindest in Erfahrung brachten um was es sich hierbei handelte was diese Geräusche gemacht hat.

### Die Höhle

![cave](../assets/images/log/session-10/cavehome.png)

Die Höhle konnte ausfindig gemacht werden und davor konnte man viel Blut sehen sowie Schleifspuren in die Höhle. Wie sich herausstellte lebte in der Höhle eine Elfe (names Eliana) welche anfänglich sehr ängstlich war und sich anschließend sehr über den Besuch von Oberflächen Bewohnern freute. Ein größer Löwe erschien wohl gestern abend mit vielen wunden und sie versucht ihn gesund zu pflegen, leider ist sie nicht wirklich eine gute Heilerin das wäre ihre Schwester (Esandra). Leider wurde ihre Schwester ebenfalls verletzt und liegt nun in einem anderem Raum in dieser Höhle. 

Jules bemerkte, durch einen Zauberspruch, das es sich um keine Elfe sondern um ein Feenwesen handelte und mit dieser Hilfe konnte sowohl Bhalrik als auch Neldor erkennen das es sich um eine Illusionäre Gestalt handelt und es in wirklichkeit um zwei Hags handelt.

Wie sich herausstellte waren es zwei Schwestern welche einfach nur in Ruhe gelassen werden wollten und hier die letzten dutzend Jahre sicher fühlen konnte da es eine Steinklippe war an der sie sich befanden. Durch das steigende Wasser ist dieser Bereich wieder zugänglich und sie müssen wohl umziehen - möchten allerdings nicht gehen ohne das sie dem Löwen geheilt haben. Der Löwe wurde wohl von einer Gruppe von Dunkelzwergen angegriffen und sein Gefährte wurde von ihnen bereits getötet. 

So unglaubwürdig die Geschichte auch klang, hatte die Gruppe das Gefühl das sie die Wahrheit sagten. Severin bemerkte zwar das es kein normaler Löwe war sondern eine Monstrosität allerdings schien sie nicht böse zu sein, daher konnte er seinen Drang zügeln das Biest zu töten.

Da die Dunkelzwerge ebenfalls eine Gefahr für die Gruppe darstellen konnte beschlossen Sie sich die Dunkelzwerge mal anzusehen. Zum Schutz gaben die zwei Schwestern der Gruppe drei Tränke mit.  Nach mehreren Stunden Wanderung konnte die Gruppe die Dunkelzwerge.

### Duergar

Die Gruppe erreichte die Höhle der Löwen und dort sollten sich auch die Zwerge befinden. Severin schlich sich in die Höhle hinein und Neldor als auch Bhalrik versuchten zu folgen, allerdings wurden bewegten sich beide so laut das Severin sie bat zurückzubleiben und er schlich allein weiter. Neldor schickte alle paar Sekunden eine Nachricht in die Richtung von Severin damit sie in Kontkat bleiben konnten und als Severin ihnen erzählte das hier sehr viele Duergar sich befanden und sie niemals einen Chance gegen sie hätten beschlossen sie zurückzugehen.

![Duergar](../assets/images/log/session-10/duergar.png)

### Abendruhe

Die Gruppe erreichten die Höhle der Schwestern und erzählen was sie sahen. Bhalrik heilte mit seiner Magie den Löwen wenn auch dieser ihm nicht sonderlich freundlich gesonnen war, allerdings konnte Bhalrik in soweit beruhigen das er sich von ihm heilen lies. Sie bekamen als Dank von den zwei Schwestern noch drei Heiltränke (zwei normale und einen großen Heiltrank). Die zwei Schwestern beschlossen das es hier nicht mehr sicher genug war und sie sich nach einem neuen Ort umschauen werden. Da es dem Löwen gut genug ging wurde dieser in die Freiheit entlassen und sollte sich vor den Dunkelzwergen in Acht zu nehmen.

![cavehome](../assets/images/log/session-10/cavehome_2.png)

Bhalrik bot den Schwestern an bei Ihnen zu bleiben ums ie vor direkter Gefahr zu beschützen und ebenfalls zu helfen ihre Spuren zu verwischen. Wenn die Schwestern weitesgehend fertig gepackt hätten sollten sie zum Rest der Gruppe kommen damit sie am Ufer gemeinsam die Nacht verbringen konnten.

Der Rest begab sich zurück zum Ufer. Severin stellte sich erneut als begnadeter Handwerker heraus und konnte mit seinen Kenntnissen die Arbeiten deutlich schneller und besser abschließen.

## Beute

| Gegenstand | Anzahl   | Bekommen von                | Charakter bekommen                          |
|-------------------|---|-----------------------------|---------------------------------------------|
| Potion of Speed                       | 1 | [Esandra](../npc/esandra.md)  | Neldor                     |
| Potion of Grwoth                      | 1 | [Esandra](../npc/esandra.md)  | Jules                      |
| Potion of Fireball                    | 1 | [Esandra](../npc/esandra.md)  | Bhalrik                    |
| - | - | - | - |
| Potion of Healing                     | 1 | [Esandra](../npc/esandra.md)  | Eldeth                     |
| Potion of Healing                     | 1 | [Esandra](../npc/esandra.md)  | Jules                      |
| Potion of Greater Healing             | 1 | [Esandra](../npc/esandra.md)  | Neldor                     |
