## Ylsa

### In Gracklstugh

- durch die Tore Gracklstugs
- steinriesen sind hier und bearbeiten stein + metal
- steinriese dreht durch

### Das Angebot

- Ylsa kann uns sicher an die oberfläche bringen für 2.000 gp / person.
- Wenn wir ihr einen [Gefallen](../quests/derro.md) tun, bekommen wir die Eskorte umsonst.


### Derro Aufstand

- derro waren sklaven der duergar
- derro sind vor weniger hunderte jahre sind sie "bürger"
- derro sammeln waffen und bereiten wohl einen aufstand vor. wir sollen die drahtzieher dahinter finden. für diesen gefallen würde sie uns kostenlos an die oberfläche bringen.
- Derro bilden einen eigenen Clan
- Rat der Diener sind die anführer der derro
- Bei einem weiteren Aufstand könnten die Derro ausgelöscht werden

### Waren über Waren

- in dem gebäuse von ylsa befindet sich auch ein warenlager von verschiedensten Gegenständen außergewöhnlicher Qualität und magischen ursprungs. (sane magical price list + 20%)
  - darunter eine mithral half-plate +1
- wir haben werz in seinem laden besucht und auch er hat hier ein ziemliche beeindruckende sammlung von gegenständen (sane magical price list + 10%).
  - darunter einen langbogen +2

## Hgraam

- Hgraam, der Steinsprecher
- Dorun, Auszubildener Steinsprecher
- Hgraam bittet uns die [Ursache für den ausbreitenden Wahn](../quests/madness.md) zu finden.

### Sarith

Sarith wenn er "träumt":

- "alles wird unterjocht werden"
- "alles gehört ihm"
- "alles breitet sich aus"
- "alles wird kontrolliert werden"
- "keiner hat eine change auszukommen"
- "jeder wiederstand ist zwecklos"
- "nein das lässe ich nicht zu - ich werde das zu verhindern wissen"

Hgraam untersucht Sarith. er sieht schmerzverzerrt aus während er ihn untersucht hat.
sarith ist wohl in einem fortschrittlichen stadium von dem wahn.

### Derendil

- erinnerungen sind für ihn wahr, allerdings gibt es eine eindeutige barriere die sein wahres ich verstecken könnten
- derendil willigt ein und lässt sich handschellen anlegen
- derendil schreit vor schmerz und zerreißt die handschellen
- neldor schlägt mit der breitseite des schwertes gegen die schläfe und derendil kommt langsam wieder zu sich
- derendil hat auch eine variante des wahnsinns, welcher den ursprünglichen selbst überschrieben