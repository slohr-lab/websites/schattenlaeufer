## Brysis von Khaem

![Brysis](../assets/images/log/session-11/brysis.png)

Nachdem Whispers von mir gerissen wurde war ich perplex. Ich hatte nicht mit so einem schnellen Angriff des Geistes gerechnet. Ich wurde durch eine gierige schrille, ja schon fast an wahnsinn grenzend, aus meiner Starre gerissen.

__"Kooommt her. Mit euren warmen vor Leben strotzenden Körpern! Ich wiiiiill euch euuurrrrree Lebenseeeenergiiiiiee entreisssssssssen!"__

"Dort unten ist ein Geisterhaftes Wesen, es hat Whispers getötet" sagte ich zu den anderen und ich nahm mein Schwert in die Hand und machte mich bereit in das Loch zu springen um es zu vernichten. Ich spürte wie sich eine Hand sanft auf meine Schulter legte. "Lass uns besonnen handeln" klang die Stimme von Severin an mein Ohr "Wir haben doch den Feuertrank und diesen könnten wir zu dem Biest reinwerfen um es zu schwächen bevor wir drauf losstürmen", vollendete er seinen Gedanken.

"Gute Idee, auf die Seite" hörte ich Bhalriks Stimme dröhnen und ich hatte kaum Zeit auf die Seite zu treten als schon eine riesige Feuersäule sich aus dem Loch erhob. Es war so grell das ich meine Augen verschließen musste, während meine Ohren die Klagen und Schmerzenschreie der Kreatur wahrnahmen. 

Ich sah noch die weißen Haare von Jules vor mir ins Loch springen und setzte sofort nach. Der Fall war nicht lange, vielleicht zwei Meter und ich konnte mich einer Rolle abfedern und sah aus den Augenwinkeln wie Jules etwas aus dem Sarkophag nahm während sich mein Blick hasserfüllt auf die Kreatur vor mir richtete und ich feuerte Instinktiv einen arkane Energie aus meiner Hand auf den dunkeln Geist schossen und der Einschlag auch ein größeres Loch in seinem Körper riss, was sich allerdings schnell wieder zu schließen begann.

Eldeth, Bhalrik und Severin landeten hinter mir und griffen auch sofort das Wesen an. Jules gesellte sich zu mir und sie hielt ein Schwert das aus Flammen, nein Licht, zu bestehen schien in ihrer Hand. Die wild loderenden Tentakeln der Lolth Peitsche schnellten nach vorne und zwei der Schlangenköpfe bissen sich auch Tief in die Geistergestalt ein während die Pfeile von Severin fast irgendwie durch das Wesen drangen ohne starke Spuren zu hinterlassen. Ich sah wie die geisterhaften überreste von Brysis von Khaem nach Jules mit ihren Krallen schlugen allerdings blockierte Jules diese mit ihrem Schild. Dies war eine Gelegenheit die ich ausnutzen musste. Meine Augen loderten vor arkaner Energie und ich konnte regelrecht sehen wie die arkane Energie auch mein Schwert umhüllte während ich nach Brysis schlug. Mein Schwester durchtrennte beide Arme und den Hals mit einem gewaltigen Hieb.

Ich begann den Raum noch genauer zu untersuchen während sich Bhalrik daran machte das Goldverziehrungen vom Sarkophag zu entfernen. Ich fand eine wohl unsichtbare Truhe am Kopfende des Sarkophag und machte mich daran sie zu öffnen. Nach einigem hin und her gelang es mir die Truhe zu öffnen und fand neben einem Haufen Gold- und Silbermünzen auch mehrere Edelsteine als auch zwei Tränke und eine schöne Halskette. Wir teilen das Gewicht auf uns fünf, da speziell die Münzen ein ganz schönes Gewicht hatten.

"Schauen wir uns noch in den anderen verschlossenen Raum um, oder gehen wir wieder nach draußen", frage ich während ich mich daran machte aus dem Loch zu klettern, dank der Hilfe von Jules war es ein leichtes für mich hochzukommen.

"Lass uns noch einen kurzen Blick riskieren und anschließend schauen wir zu den anderen", antwortete Jules als sie ebenfalls als letzte oben angekommen war.

## Falsches Grab

Der weitere Raum stellte sich als falsche Grabkammer heraus. Es gab dort nichts von Interesse. 

## Das Ende der Reise

Wir alle beschlossen eine kurze Rast einzulegen und ich identifizierte die gefundenen Gegenstände. Bei dem Schwertgriff, den Jules gefunden hatte, handelte es sich um ein ziemlich mächtiges Schwert mit dem Namen _Dawnbringer_. Das Schwerte hatte Jules auserkoren sich von ihr führen zu lassen. Die Halskette war ein _Necklace of Fireballs_ welches Bhalrik an sich nahm. Es wurde beschlossen das die Tränke wie auch die Edelsteine am besten ich an mich nehme.

Nach einer kurzen Verschnaufpause machten wir uns auf die Reise Richtung unseres momentanen Zwischenziels. Hemeth schien zuversichtlich das wir bald ankommen werden, da er diese Höhlen sehr gut kannte. Er lenkte das Boot in einen kleinen Seitentunnel und schlug vor hier uns gut auszuruhen bevor wir nach Gracklstugh kommen würde. Von dieser Position aus wären wir in unter eine Stunde ein Gracklstugh.

![Worldmap](../assets/images/log/session-12/worldmap.png)

## Beute

| Charakter     | art                   | Menge | Gegenstand                | pp    | gp    | sp    | cp    |  ort      | 
|---------------|-----------------------|------:|---------------------------|-------|-------|-------|-------|-----------|
| Bhalrik       | :material-minus-box:  | 1     | Potion of Fireball        |       |       |       |       | Lost Tomb |
| Jules         | :material-plus-box:   | 1     | Dawnbringer               |       |       |       |       | Lost Tomb |
| Neldor        | :material-plus-box:   | 11    | Zirkon (50gp)             |       |       |       |       | Lost Tomb |
| Neldor        | :material-plus-box:   | 1     | Greater Healing Potion    |       |       |       |       | Lost Tomb |
| Bhalrik       | :material-plus-box:   | 1     | Necklace of Fireballs     |       |       |       |       | Lost Tomb |
| Plop          | :material-plus-box:   | 1     | Lump of Gold (250 gp)     |       |       |       |       | Lost Tomb |
| Neldor        | :material-plus-box:   | 1     | Philter of Love           |       |       |       |       | Lost Tomb |
| Bhalrik       | :material-plus-box:   |       |                           |       | 240   | 800   |       | Lost Tomb |
| Neldor        | :material-plus-box:   |       |                           |       | 240   | 800   |       | Lost Tomb |
| Eldeth        | :material-plus-box:   |       |                           |       | 240   | 800   |       | Lost Tomb |
| Jules         | :material-plus-box:   |       |                           |       | 240   | 800   |       | Lost Tomb |
| Severin       | :material-plus-box:   |       |                           |       | 240   | 800   |       | Lost Tomb |
