## Downtime

### Gegenstände herstellen.

- Bag of Holding
- Moon Sickle

### Informationsbeschaffung

- In der Schlucht sind die Derro zu Hause
- Starker Smog ist in der Schlucht
- Die Informationsbeschaffung von Jules und Severin waren/sind nicht sonderlich erfolgreich
- mit "speak with animals" und "Beast Sense" wurde durch Sev herausgefunden das im Westen der Schlucht mehr "verkehr" ist.

## Keeperes of the Flame

Themberchaud hat uns eingeladen direkt.

Duergar Anführer der Keepers: Gartokkar

- wir werden in den süden geführt. der drache holt uns in seine dienste genommen und wir sollen ihm direkt bericht erstatten
- grey ghost haben das ei gestohlen
- concuil der diener stecken wohl dahinter
- droki ist wohl ein drahtzieher auch dahinter
- belohnung bekommt man etwas aus dem Drachenhort
- bekommen siegel von Laduguer um uns frei in der stadt bewegen zu können.

## Laduguer’s Furrow (Schlucht)


## Droki
- Boots of Speed
- waffen kaufen ist ein auftrag
- Uskvil beauftragt droki
- tunneln außerhalb von gracklstugh (Whorlstone Tunnels)
- Uskvil für waffen ist der gleiche als für die grey ghost
- Grey Ghosts befinden sich in den Whorlstone Tunnels
- beschafft schon waffen seit monaten
- magischen sack (sieht unmagisch aus und innen wohl größer als außen)
  - 1 gp
  - 10 sp
  - 3 scrolls (see invisible)
  - 2 potions of healing
  - stapel von echsenhaut pergament (spellbook)
    - tensors floating disk
    - fiegn death
  - verschiedene verwesungsstufen insekten, spinnen
  - ein faustgroßer klumpen metal
  - kupferrohr für spellscrolls, mit zehennägel und hautschuppen
  - liste mit vier namen (darunter Dorhun)
  - nägel sind für kultisten gedacht
  - kultisten befinden sich in den Whorlstone Tunnels
  - Die Whorlstone Tunnels befinden sich hinter dem West Cleft District Tor.