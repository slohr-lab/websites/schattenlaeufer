## befragung von narrak

- speak with dead


## höhle der kultisten

- zeichnung des dämonischen zirkels angefertigt
- quasit sind in dem kleinen ausgang. sie beobachten uns und während wir aus der höhle gehen.

## gang tunnel
- eine steinplatte ist in den einem gang eingelassen um einen gang nach norden abzusperren


## grüner pool
- water weird im pool
- greift an nachdem eldeth das wasser grün färbt
- bhalrik tötet das water weird mit scorching ray

# labor
- lorthio bukbukken alchemist
- tötet sich selber bevor er fragen 
  - fragen an ihn:
      - wo ist uskvil - oben
      - wo ist "oben" - deutet auf eine tür
      - sind hier tränke - ja
      - sind hier in den tunneln weitere grey ghosts - ja
- flasche mit gift vom alchemisten eingepackt

- im schreibtisch verstecktes fach mit brief
  - "I don’t need your poison anymore. I’ll deal with Werz Saltbaron myself. Bring me an elf blade, one with the swirlies carved on the steel, and I’ll forget you failed me. And I don’t want to see any of your goons near my post. The captain is poking around, and I could use a scapegoat. – Gorglak"
- zwei herbalism kits
- ein posioners kit
- 10 healers kit
- 10 alchemisten feuer
- 6 säure flaschen
- 2 potion of healing
- 1 potion of greater healing
- 1 potion of fire breath
- 1 potion psychic resistance
- zerissene notizen mit arkanen symoblen (rezept für alchemisten feuer und säureflaschen aus pilzen)
- 7 bigwigs
- 2 pigwigs

## kultisten tierpark
- drei cave bears getötet
- einen derro getötet
- einen derro gefangen genommen