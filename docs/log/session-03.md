## Ein schlechtes Erwachen

Nach einer anstrengenden Reise saßen wir im Kreis um ein kleines Feuer, welches [Bhalrik](../pc/bhalrik.md) in der Mitte der Gruppe erschaffen hat. [Neldor](../pc/neldor.md) begann die Untergrundbewohner nacheinander auszufragen über die umliegenden Städte deren Einwohner und auch generell anderen interessanten Gegebenheiten und Bräuche.

Da es ein anstrengender Tag war legte [Neldor](../pc/neldor.md) sich nach dem Gespräch hin und der Schlaf überkam ihm sofort. Es war diesmal kein traumloser Schlaf.

??? note "Neldors Traum"
    Er fiel einen dunklen und schier endlosen Tunnel hinab. Die Tunnel erinnerten ihn sehr stark an die Höhlen durch jene er den ganzen Tag wanderte. Er konnte einen Lichtblick in weiter ferne ausmachen und dieser wurde schnell größer. Das Licht nahm das halbe Sichtfeld ein und er konnte nun erkennen das er aus hoher Höhe auf eine bizarre, aber vertraute, Welt schaute. Das Licht kam von einer Vielzahl unterschiedlich großer Lavaflüsse welche sich durch dunkelrotes Gestein fraß. Halb vom Lava zerfressene Ruinen kamen zum Vorschein als er immer schneller fiel. Nun konnte er auch eine Vielzahl von grotesken Kreaturen erkennen. Sie bemerkten ihn und blickten gierig nach ihm. Seine Hand klammerte sich den Griff ein seiner Hand immer fester. Das Gefühl des Griffes beruhigte ihn auch wenn er seinem sicheren Tod in die Augen starrte. Er schoss noch immer auf den Boden zu und dieser war noch gute 100 Meter entfernt. 

    ![Nine Hells](../assets/images/log/session-03/hells.jpg)

    "Du bist auf den richten Weg, komm zu mir und gemeinsam können wir ihn überkommen!" vernahm er in seinem Kopf bevor er mit unglaublicher Kraft zurück gerissen wurde und aufwachte. 
    
Er merkte das er vor Überraschung laut aufgeschrien hatte und blickte sich um. [Tessaia](../pc/tessaia.md) schien ihn gehört zu haben, darum drehte er sich schnell wieder um und versuchte zu schlafen. Was war dies für eine Stimme und vor allem das Gefühl der Zuversicht welches er schon so lange nicht mehr gespürt hatte.

Dann wurde [Neldor](../pc/neldor.md) in weiteres Mal geweckt, er hatte gar nicht mitbekommen das er während des Grübelns wieder eingeschlafen war. Die Hand von [Tela](../pc/tela.md) auf seiner Schulter ihn und schüttelnd. [Neldor](../pc/neldor.md) dachte im ersten Moment das die normale Wachablösung gekommen war allerdings irgendwas stimmte in ihrem Gesicht nicht. Der Ausdruck war sehr kalt und leicht befremdlich. Die Ursache hierfür war schnell gefunden, als [Neldor](../pc/neldor.md) über die anderen Gefährten schaute welche nun ebenfalls geweckt wurden, alle bis auf Einen: [Jimjar](../npc/jimjar.md). Dieser lag mit ausgebreiteten Armen und Beinen an jener Stelle wo er sich niedergelegt hatte. Seine Kehle war durchgeschnitten und seine Eingeweide waren jeweils von seinem Bauch zu seinen Armen gezogen worden. Es war ein wahrlich grotesker Anblick.

"War ich das?", "Hat das was mit meinem Traum zu tun?", "Ist es wieder passiert?" waren die Gedanken die [Neldor](../pc/neldor.md) durch seinen Kopf geschossen sind während er sich mit dem Rücken an einen größeren Felsbrocken sinken lies. Dies hat [Tessaia](../pc/tessaia.md) wohl bemerkt da sie ihn umgehend darauf ansprach ob er etwas gemerkt habe bzw. ob dies etwas mit seinem Albtraum zu tun haben könnte.

Es dauert etwas bis [Neldor](../pc/neldor.md) sich wieder unter Kontrolle hatte er ging auf die Leiche und sah aus den Augenwinkeln das es wohl alle ziemlich getroffen hatte. [Jules](../pc/jules.md) und [Severin](../pc/severin.md) saßen katatonisch vor der Leiche während [Tela](../pc/tela.md) versuchte [Tessaia](../pc/tessaia.md) zu beruhigen. [Bahlrik](../pc/bhalrik.md) kümmerte sich um [Jules](../pc/jules.md) und [Severin](../pc/severin.md) während [Neldor](../pc/neldor.md) anfing sich der Ausrüstung von [Jimjahr](../npc/jimjar.md) zu entlegen. Immerhin kann der Leichnahm damit nichts mehr anfangen. Er versuchte die Waffen wie auch die blutverschmierte Rüstung den unbewaffneten und umgerüsteten Mitreisenden anzubieten aber sie lehnten ab. Der Leichnam kann damit nichts mehr anfangen und wir können dies immerhin noch zum tauschen verwenden. Er nahm, soweit er die Ausrüstung tragen konnte, an sich und den Rest nahm [Bahlrik](../pc/bhalrik.md) an sich.

Es wurden mehrere Untersuchungen angestellt allerdings konnte man weder Spuren oder anderweitige Informationen sammeln außer das offensichtliche - ein oder mehrere Personen hatten den Mord begangen und es war wohl etwas rituelles da keinerlei Wertgegenstände mitgenommen wurden. Der Großteil der Mitreisenden glaubte das die Gefahr von außerhalb der Gruppe kam. 

Anschließend verbrannten sie den Leichnam und marschierten noch eine gute Stunde bevor sie sich erneut zur Ruhe setzten um den nächsten Tag wieder ausgeruht weitermarschieren zu können. [Neldor](../pc/neldor.md) schlief gut weiter, allerdings hat wohl nicht jeder in der Gruppe gut geschlafen.

## Geräuschlos

Der nächste Reisetag zog sich ganz schön in die Länge. Wir mussten durch viele sehr kleine und sehr enge Tunnel gehen bzw. kriechen. [Bahlrik](../pc/bhalrik.md) bemerkte als erstes ein Kreischendes / Weinendes Geräusch das von hinter der Gruppe kam. Wir versuchten so schnell wie möglich weiterzukommen damit wir nicht einzeln gegen was auch immer da kämpfen müssen. Das Geräusch kam immer näher und schließlich holte es uns ein. Es ist war nicht auszumachen was es war, allerdings schien es eine unsichtbare Macht zu sein welche einmal über alle Reisenden hinwegzufliegen. [Severin](../pc/severin.md) wurde von dem Geräusch taub. [Jules](../pc/jules.md)  kümmerte sich [Severin](../pc/severin.md) während [Eldeth](../npc/eldeth.md) seine Aufgabe des spuren Verwischens primär übernahm.

## Unkluges Handeln

![Plop](/assets/images/characters/plop.jpg){: align=right width=300}

Nach einiger Zeit wurde der Weg wieder etwas begehbarer und dann öffnete er sich wieder in ein größeres Höhlensystem im dem es auch wieder vereinzelnde größere Pilze gab. [Bahlrik](../pc/bhalrik.md) nahm Bewegung voraus und die Gruppe beschloss sich so schnell wie möglich zu verstecken. Ihnen kam ein Trupp von sieben Dunkelelfen auf vier riesigen Echsen entgegen. Die Dunkelelfen schienen die Gruppe bemerkt zu haben da sie laut ausriefen "Wer ist da!". [Neldor](../pc/neldor.md) überkam ein Gefühl der Entschlossenheit und schritt nach vorne mit [Bhalrik](../pc/bhalrik.md) im Schlepptau und ging auf die Dunkelelfen zu. Bevor er noch hinter dem großen Pilz hervortrat erschien ein Bild von ihm als Dunkelelf mit den Hausinsignien der Dunkelelfen aus [Velkynvelve](../locations/velkynvelve.md).

[Neldor](../pc/neldor.md) konnte die Dunkelelfen nicht nur davon überzeugen das er den "Oberflächen" Bewohner gefangen genommen hatte und auch auf den Weg zurück nach [Menzoberranzan](../locations/menzoberranzan.md) sich befindet sondern auch ihnen Nahrung sowie einen der Echsen als Reittier zu überlassen. Erleichtert und zufrieden mit sich, hier einen unnötigen Kampf vermieden zu haben, wurde er von [Sarith](../npc/sarith.md) gescholten das dies keine gute Idee gewesen ist und es viel sicherer für sie gewesen wäre die Drows zu eliminieren damit diese nicht unseren Verfolgern hinweise geben können wohin wir reisen.

Als sie eine sichere kleine Höhle in der Nähe eines Flusses erreicht hatten beschlossen sie eine Rast zu machen. Das Wasser des Flusses wars eiskalt und aus einem, [Neldor](../pc/neldor.md) unbegreiflichen Grund, beschloss der Großteil der Reisenden ein Bad zu nehmen. Da es eh schon kalt genug war kuschelte [Neldor](../pc/neldor.md) sich an die Innenseite von [Plop](../npc/plop.md) (er beschloss die Echse so zu nennen, da ihre füße ein leises "Plop" von sich gaben wenn sie sich vom Boden lösten).

## Spinnenschlucht

![Spinnenschlucht](../assets/images/log/session-03/ravine.jpg)

Auch an diesem Tag kamen sie nicht sonderlich gut voran da da ihre Reise sie an eine ziemlich große Schlucht welche mit dicken und dünnen Spinnennetzten überfüllt war. [Bhalrik](../pc/bhalrik.md) erfuhr, dank seiner Gabe mit Tieren sprechen zu können, das [Plop](../npc/plop.md) hier schon öfters durchgekommen ist auf den Reisen mit den Händlern, wenn auch sich hier viele nicht gut schmeckende Spinnen herumtrieben. Darum beschloss die Gruppe das es wohl ein sicherer Weg sei sich über die Spinnennetze zu begeben. Wie zu erwarten war wurden sie auf halbem Weg über die Spinnweben von mehreren Riesenspinnen angegriffen. Der Angriff wurde schnell zurückgeschlagen nicht zuletzt dadurch da alle Mitglieder der Party jetzt auch Waffen besaßen sondern mal wieder durch die Wut und Angriffskraft von [Ront](../npc/ront.md) welcher im Alleingang zwei Spinnen zu Spinnenmatsch verarbeitete. Die Schlucht bereitete sich über mehrere Meilen aus und zwischen drin waren lediglich ein paar Stalaktiten oder Stalagmiten welche als zusätzlich Untersetzung für das Netz war. Aber sie erreichten ohne weitere Zwischenfälle das andere Ende der Schlucht.

## Eine unausgesprochene Warnung

![Rune](/assets/images/log/session-03/demon-ward.png){: align=left width=300}

Als die Erschöpfung die Gruppe eingeholt hatte beschlossen sie eine weitere lange Rast einzulegen. [Severin](../pc/severin.md) bemerkte eine Rune auf einen Stein eingelassen war. [Sarith](../npc/sarith.md) erklärte der Gruppe das es sich hierbei um eine Warnung handelte das sich in dieser Gegend Dämonen herumtrieben. Die Gruppe beschloss noch gute zwei Stunden weiterzureisen und dann eine lange Rast zu machen, da sie hier die Warnung des Dunkelelfen ernst nahmen. 

Während des Abends erfuhr die Gruppe das [Stool](../npc/stool.md) in der Lage war ein Telepathisches Netz mit mehreren Kreaturen herzustellen und [Jules](../pc/jules.md) überredete ihn dies mit allen anderen Oberflächenbewohnern ebenfalls zu tun. Nun waren auch alle wieder in der Lage sich mit [Severin](../pc/severin.md) zu unterhalten da dieser immer noch an den Folgen der Taubheit litt.

## Opfergaben

![Runde 1](../assets/images/log/session-03/round-01.png)

Die Nächsten Tage und nächste waren weitestgehend Ereignislos. Die Gruppe beschloss sich möglichst schnell zu bewegen auch wenn es dadurch schwieriger ist die Spuren zu verwischen oder die Ausschau für Gefahren zu halten, allerdings wollte man so schnell wie möglich in die nächste Stadt [Sloobludop](../locations/sloobludop.md) kommen. Am Ende des siebten Reisetages erreichten sie einen Wasserfall der über mehrere Ebenen ging. Hier wurden sie von mehreren Kuo-Toa angegriffen welche alle "Neue Opfergaben" schrien bevor sie auf die Gruppe zustürmte.

![Runde 2](../assets/images/log/session-03/round-02.png)

[Neldor](../pc/neldor.md) der noch immer auf [Plop](../npc/plop.md) ritt konnte ohne größere Problem mehrere Meter hohen Klippen klettern und war leider auch schnell eingezingelt von mehreren Koa-Toa und dann auch noch in eines ihrer Klingennetzte gefangen. Während der Kampfmittelpunkt [Derendil](../npc/derendil.md) war da sich hier die meisten Kuo-Toa versammelt hatten. [Tessaia](../pc/tessaia.md) konnte einen besonders gefährlich aussehenden Kuo-Toa mit einem Zauber dazu zwingen das er laut lachend am Boden sich hin und her geworfen hat.

Nachdem [Derendil](../npc/derendil.md) mit mehreren schnellen Schlägen auf den am Boden liegenden Kuo-Toa eingeschlagen hat kam dieser auch aus seiner Lachattacke raus und erschlug mit einem schnell schlag den Quaggoth und machte sich stellte sich nun [Eldeth](../npc/eldeth.md) in den Weg. Nachdem [Tela](../pc/tela.md), [Turvy](../npc/topsy_turvy.md), [Buppido](../npc/buppido.md) und [Ront](../npc/ront.md) mehrere Kuo-Toa welche [Neldor](../pc/neldor.md) umzingelt hatten erschlagen hatten konnte [Neldor](../pc/neldor.md) sich auch etwas weiter in Sicherheit zurückziehen und lediglich mit Magie die anstürmenden Fischmenschen angreifen, welche immer noch aus dem Höhleneingang strömten.

![Runde 3](../assets/images/log/session-03/round-03.png)