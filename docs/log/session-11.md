## Entkommene Beute

Während unserer langen Rast kamen kam Wisphers zurück und weckte Neldor und informierte ihn das sie demnächst von einem vielen Duergar besuch bekommen würden. Wie es schien jagten die Zwerge wieder den Löwen und der Löwe bewegte sich zielstrebig auf sie zu. Mit einem kleinen Fluchen stand Neldor auf und beriet sich mit seinen Weggefährten über die nächsten Schritten.

Eliana und Esandra bestanden darauf den Löwen erneut zu helfen während Jules und Eldeth einen Konflikt lieber vermeiden wollten. Man einigte sich darauf so gut es ging dies friedlich zu lösen und erneut wurden Bhalrik und Eldeth als Dunkelzwerge geschminkt in der verbleibenden Zeit bis sie kamen. Neldor verwandelte sein Äußeres um wie ein großer gefährlicher Dunkelzwerg auszusehen und während die anderen alle sich versteckten und Stellung bezogen damit sie im Falle der Fälle schnell eingreifen konnten.

Der Löwe wurde von Whispers angelockt und wurde dann von den zwei Hags versteckt während sich Bhalrik, mit seinem beschworenen Bären, Eldeth und Hemeth zusammen mit Neldor bereit machten die Duergar zu empfangen. Die Duergar liesen die Gruppe auch nicht lange warten. Die Situation war von Anfang sehr angespannt und es dauerte nicht lange bis Sie in Waffengewalt eskalierte.

![Duergar](../assets/images/log/session-11/duergar-01.png)

In den ersten Sekundne wurde der Anführer der Duergar von mehreren Mitgliedern der Gruppe getroffen allerdings von Eldeth niedergestreckt. Einige der Duergar ergriffen daraufhin die Flucht und somit war der Kampf bereits gewonnen. Es dauerte nicht lange bis die noch nicht an Ort und Stelle getöteten auch die Flucht ergriffen. Derendil setzte ihnen nach und Neldor als auch Bhalrik halfen ihm dabei da sie nicht wollten das er noch schwerer verwundet wurde als er bereits war.

Als nennenwerte Beute sei ihr die Platte sowie der Kriegshammer des Anführers der Dunkelzwerge zu nennen.

Im Anschluss nach diesem Kampf konnten alle rasten währen die Wachen nach weiteren Duergar ausschau hielten. Am nächsten "Morgen" machten sich die zwei Eliana und Esandra zusammen mit ihrem Löwen zurück in ihre Höhle da ihnen unmitelbar keine Bedrohung mehr durch die Dunkelzwerge bevorstand.

Die restlichen machten sich auf den Weg mit ihrem neuen Boot Richtung Gracklstugh.

## Darklake

Die nächsten Tage waren sehr angenehm auf dem Darklake. Sie reisten in einer sehr großen Höhle in der es immer wieder größere Insel mit Pilzen gab auf denen Sie Rast machen konnten. 

## Grabkammer: Brysis von Khaem

Am dritten Tag ihrer Reise hörten Sie eine Stimme in ihrem Kopf welche nach Hilfe rief.  Sie beschlossen den Hilferufen nachzugehen und kamen zu einer Steinernen Tür welche in eine Mauer eingelassen war. In diese Tür begaben sich Eldeth, Jules, Bhalrik, Severin und Neldor. 

![Khaem](../assets/images/log/session-11/khaem.png){: align=left width=250}

Im ersten Raum befanden sich Malereien welche auf ide Stadt Khaem hindeutete. Es war eine alte Stadt die schon seit jahrhunderten nicht mehr existierte, wie sich Neldor erinnerte. ES war noch zur Zeit als die mächtigen Magier Städte in die Lüfte erhoben und durch die immer mächtigeren Zauber beinahe das arkane Netz des gesamten Kosmos zum zerfall brachten. Dies war wohl die Grabkammer von Brysis aus Khaem einem mächtigen Zauberer der hier mit seinen Gefährten begraben wurde.

Sie machten sich weiter die Grabkammer zu erforschen und den Ursprung der Stimme zu finden. Severin vermutet nichts Gutes in der Grabkammer und hatte Angst das sie dabei sind einen Dämon freizulassen. Neldor und Jules versicherten ihm das wenn es sich dabei um einen böses wesen handeln sollte, es nicht freigelassen werden würde. Immerhin schien es ja eingesperrt zu sein und lediglich durch das erscheinen von ihnen es noch lange zu dirket zu einem Ausbruch kommen wird.

Sie kamen in eine Kammer mit mehrere Sarkophagen. Beim berühren eines der Sarkophag durch Neldor erhoben sich ein Geisterhaftes Wesen aus jedem der Ruheplätze. Die Geistern trafen Bhalrik stärker allerdings stellten Sie für die Kameraden keine wirkliche Herausforderung dar. In den Sarkophagen waren einige Wertgegenstände und sie fanden auch einen geheimen Durchgang der in eine weitere Kammer führte.

Neldor schickte Wisphers die Kammer hinein und das letzte was Neldor durch die Augen seines Vertrauten sehen konnte waren die Klauen eines größeren und gefährlich ausschauenden Geisterwesen das sich auf ihn zubewegten. Neldor erholte sich noch als sie alle die gleiche Stimme von vorhin in ihrem Kopf hörten, allerdings klang sie jetzt deutlich aggressiver und lüsterte nach der Lebensenergie der Gruppe.

![Brysis](../assets/images/log/session-11/brysis.png)

## Beute

| Gegenstand | Anzahl   | Bekommen von                  | Charakter bekommen                    |
|-------------------|---|-------------------------------|---------------------------------------|
| __30 gp__         | - | Duergar                       | Party                                 |
| Shield            | 9 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| Javelin           | 9 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| Warpike           | 6 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| Short Sword       | 3 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| Dagger            | 3 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| Studded Leather   | 6 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| Scale-Armor       | 6 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| Chain mail        | 3 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| Plate             | 1 | Duergar                       | [Eldeth](../npc/eldeth.md)            |
| Warhammer +1      | 1 | Duergar                       | [Eldeth](../npc/eldeth.md)            |
| Fließ vom Löwen   | 1 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| Acid Vail         | 6 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| Alchemist's Fire  | 6 | Duergar                       | [Plop](../npc/plop.md) / Boot         |
| - | - | - | - |
| gold censer with platinum filigree (250 gp)                                               | 1 | Lost Tomb     | [Plop](../npc/plop.md) / Boot         |
| ewer made of beaten gold (25 gp)                                                          | 1 | Lost Tomb     | [Plop](../npc/plop.md) / Boot         |
| walking stick made of varnished yew with a golden handle shaped like a scorpion (75 gp)   | 1 | Lost Tomb     | [Plop](../npc/plop.md) / Boot         |
| gold bracelets (50gp)                                                                     | 2 | Lost Tomb     | [Plop](../npc/plop.md) / Boot         |
| ceremonial wand (nonmagical) made of chiseled ivory (25gp)                                | 1 | Lost Tomb     | [Plop](../npc/plop.md) / Boot         |
| onyx ring (50 gp)                                                                         | 1 | Lost Tomb     | [Plop](../npc/plop.md) / Boot         |
| a silver necklace set with two azurites and a carnelian (250 gp)                          | 1 | Lost Tomb     | [Plop](../npc/plop.md) / Boot         |
