## keepers of the flame

drachenei abgegeben im austausch für

- 20.000 gp + bonus
  - longbow +2 (-> sev)
  - endless quiver (-> sev)
  - 2x ring of protection (-> neldor; bhalrik)
  - ring of evation (-> jules)

## darklake district

- mit dem Captain der Zwergenwache gesprochen und den bestechlichen Gorglak überführt
- anerkennung des Captains verdient

## dankbarer steinsprecher

- dem steinsprecher wurde narrak übergeben (was halt noch davon überig war)
- das ritual das die riesen in den wahnsinn trieb wurde unschädlich gemacht
- seine dankbarkeit haben wir gewonnen und er schuldet uns einen gefallen für die zukunft

## teleportation circle

- keeper of the flames haben einen und neldor durfte sich die runen notieren