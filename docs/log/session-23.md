## angriff auf das herz des hains

![Yggmorgus](/assets/images/log/session-23/yggmorgus.jpg){: align=right width=500}

die gruppe beschließt sich das herz des hains näher anzuschauen und wird von einer gruppe von mutierenden myconid als auch sarith. die gruppe kan noch sehen wie der kopf von sarith explodiert und er sich schnell in einen mit pilzen überwuchertem Etwas verwandelt. 



Yestabrod sendet seltsame sporan Richtung Gruppe aus und es kommt zu einem kurzen aber heftigen kampf.

das groteske bild des rießigen pilzes welches umzingelt ist von hunderten wenn nicht tausenden in verschiedenen verwesungsstadien befindlichen leichen und um den pilz tanzen mehrere dutzend wesen.

auf dem weg zum Eingang des großen Pilzes entdeckt die Gruppe eine halbtote Drow welche wohl mit ihrem Trupp hier auf die Gruppe gewartet hatte, allerdings sind sie dem Einfluss von zuggtmoy zu opfer gefallen.

Sie bitte die Gruppe sie zu erlösen und was die gruppe tut.

Beute:

- studded leather +2 (drow) -> Bhalrik
- short sword +2 (drow) -> Severin
- bag of holding -> Bhalrik
- spell scroll, remove course -> Neldor
- spell scroll, spider climb -> Neldor

![Battle-1](/assets/images/log/session-23/battle-1.png)

## mit tanzen kommt man nicht weit

![Battle-32](/assets/images/log/session-23/battle-2.png)

Um den großen Pilz Yggmorgus tanzen mehrere dutzende dieser Sporenwesen. Es war unmöglich sich zum Eingang des Pilzes zu bewegen ohne an ihnen vorbei zu müssen. Es wurde gezielt gewartet eine große Ansammlung auf einem Haufen war und dann wurde aus sicherer Entfernung die Wesen vernichten.

## tote braut

![Zuggtmoy](/assets/images/log/session-23/zuggtmoy.jpg)

Als  man endlich zum großen Pilz gekommen ist konnte man die große Dame Zuggtmoy in ihrer vollen Pracht erkennen. Sie war wie an dicken Seilen (welche an einem Schleier erinnerten) in der Innenseite aufgehangen und mehrere Wesen schienen sich um sie zu kümmern. 

Neldor drängte darauf den Ort sofort zu verlassen, allerdings überredete Sev die Gruppe einen Angriff zu riskieren, da die Lady zu schlafen schien und sie somit den Überraschungseffekt auf ihrer Seite hätten.

![Battle-3](/assets/images/log/session-23/battle-3-1.png)

Den Initialschlag eröffnete Severin und Neldor ging wie die anderen in den Angriff über. Neldor blinkte immer nach den angriffen in die etherial plane und konnte somit fast allen angriffen von zyggtmoy ausweichen. Severin konnte immer aus Reichweite aller angriffe bleiben somit konzentrierten sich die Angriffe auf Jules und Bhalrik.

Kurz bevor Severin Zuggtmoy mit seinen Pfeilen niederstrecken konnte sie Jules tödlich verletzten. Mit der Gnade der Ravenqueen konnte Bhalrik wenig später ihre Seele wieder in ihren Körper zurückholen.

![Battle-3](/assets/images/log/session-23/battle-3-2.png)
![Battle-3](/assets/images/log/session-23/battle-3-3.png)

nach fast einer minute kampf wurde Zyggtmoy besiegt  und alle ihre noch verbliebenen Untergebenen blieben wie regungslos stehen. der großteil von ihnen zerfiel - ähnlich wie Zuggtmoy.