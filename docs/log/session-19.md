## zombie höhle
- hand vom geist gefunden
- Pelek’s obsidian ring

## flüsterkammer
- bhalrik hört stimmen aus dem stein
- die gruppe hört zu
- neldor befindet sich gehör-technisch in einem gracklstugh von vor einigen jahrhunderten; kann leider aber keine hilfreichen informationen extrahieren.


## spinnenköning
- kleiner gang führt in eine höhle
- zwei große spinnen sowie der spinnenkönig (große spinnen mit zwei humanioden köpfen)
- sev wird schwer verwundet
- sev möchte mehrere klein sowie großmach pilze von neldor - der sie ihm gibt

## zum drachenei
- sev weigert sich bhalrik einen pilz zu geben
- neldor zaubert remove curse und jules lesser restoration auf sev - beides bringt allerdings nichts.
- hell erleuchtete höhle mit einem obelisken und einem drachei
- in der höhle befindet sich auch ein spectator und einem derro 
- spectator hat die gruppe entdeckt und schreit
- spectator geht auf geheiß des derros direkt in den angriff über
- derro schießt einen blitz ab und verletzt neldor und jules stark und begibt sich hinter dem obelisken in deckung
- neldor trinkt einen heiltrank und rennt in deckung
- jules geht k.o.
- sev töten spectator
- bhalrik verletzt die derro stark mit scorching rays
- neldor casted armor of agathys und versteckt sich seitlich vom obelisken

- stück holzkohle
- 1 gp (uralte münze von oberflächen elfen)
- notizbuch (zwergisch)
  - eine liste von gegenständen

- neldor packt drachenei in die bag of holding

- der obelisk kann mit magie infused (1 spell slot) werden und löst damit einen teleportationszauber auf der alle auf der plattform - vermutlich - nach gracklstugh teleportiert
