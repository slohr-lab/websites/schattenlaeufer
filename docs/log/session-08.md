## Landmasse

Es begann eine Diskussion wie man weitermachen sollte nachdem man nun [Tela](../pc/tela.md) und [Tessaia](../pc/tessaia.md) verloren hatte. [Jules](../pc/jules.md) nahm das Musikinstrument von [Tessaia](../pc/tessaia.md) an sich während [Neldor](../pc/neldor.md) sich das Buch von [Tela](../pc/tela.md) näher anschaute.

Nach der langen Rast machte sich [Severin](../pc/severin.md) alleine auf um die weitere Strecke zu begutachten. Wie es aussieht müssten wir die Boote über weite Strecken eines ausgetrockneten Arms des [Darklakes](../locations/darklake.md) bringen. Hier stellt sich vorallem [Jules](../pc/jules.md), [Derendil](../npc/derendil.md) und [Plop](../npc/plop.md) als sehr hilfreich heraus da sie mit ihrer Stärke weitesgehend die Boote auch durch enge Stellen heben konnten.

[Severin](../pc/severin.md) bildete die Vorhut und blieb weitesgehend in der Reichweite des Sporennetzwerkdes (120 ft).

## versunkene Tempel

Nach mehreren Stunden marschieren, heben, zeihen ertönte die Stimme von [Severin](../pc/severin.md) im Sporennetzwerk und teilte der Gruppe mit das er Bewegung direkt voraus wahrgenommen hat und es sich hierbei um Duergar handelt.  

Leider blockierten die Duergar den Weg welchen die Gruppe nehmen musste und wie es aussah würden sie noch länger bleiben.

![Tempel](../assets/images/log/session-08/temple.png)

Eine friedliche Lösung sollte gefunden werden und [Neldor](../pc/neldor.md) verwandelte sich ebenfalls in einen Duergar und marschierte auf sie zu.  Es stellte sich heraus das die Gruppe der Duergar auf einen versunkenen Tempel gestoßen ist und hier ein Problem mit einem Troll hat.

Neldor gab an mit mehreren Duergar zu reisen und ein paar anderen ehemaligen Sklaven der Drow. Alelrdings das sie sich gegenseitig helfen könnten.

Zurück bei der Gruppe verwendete [Neldor](../pc/neldor.md) das Disguese Kit von [Tessaia](../pc/tessaia.md) um [Bhalrik](../pc/bhalrik.md) und [Eldeth](../npc/eldeth.md) ebenfalls wie Duergar aussehen zu lassen. [Hemeth](../npc/hemeth.md) war beeindruckt mit der Leistung von ihm.

Zu viert machten sie sich auf den Weg mit dem Warlord des Duergartrupps zu sprechen. __[Malgron Ironreach](../npc/malgorn.md)__ stellte sich als vernünftiger Duergar heraus. Es konnte ein Deal gemacht werden dass bei der Unterstützung der Duergar die Gruppe 1/3 allen Schatzes in dem Tempel bekommt wenn sie ihnen helfen einen großen Troll zu besiegen.

Es befanden sich ein gutes Dutzend Duergar im Tempel und die Gruppe nahm den Handel an da man nicht gegen sie kämpfen wollte, wenn es sich vermeiden lässt.

## Troll

Die Duergar benötigten noch eine Stunden Vorbereitung bevor sie mit ihrem Angriff auf den Troll starten wollten und darum legte die Gruppe eine kurze Rast ein um sich selber für den Angriff vorzubereiten.

![Round-01](../assets/images/log/session-08/round-01.png)

Bhalrik öffnete die Tür und stürmte rein während zeitgleich Severin einen Pfeil abschoss. Eldeth und Neldor stürmten gleichzeitig durch die Tür und Neldor begab sich direkt in den Nahkampf mit dem Biest. Er sah das er bereits aus mehreren Wunden dickes grünes Blut floss. Der Boden unter dem Troll war mit den vertrauten Flammen von Bhalrik bedeckt die nach ihm schnappten.

![Round-02](../assets/images/log/session-08/round-02.png)

Jules, Hemeth und Eldeth gesellten sich neben Neldor und der Troll wurde von mehreren Duergar ebenfalls eingekesselt. Hemeth und die Duergar wurden mit den schweren Pranken des Trolls getroffen bevor. Mit einem gewaltigen Hieb öffnete Jules den halben Brustkorb des Biestes und Severin schoss ihm den finalen Pfeil in sein rechtes Auge bevor der Troll auf den Flammen zusammenbrach und langsam verkohlte.

## Plünderung

![Plate](../assets/images/log/session-08/armor.jpg){: align=right width=300}

[Malgorn](../npc/malgorn.md) hielt sich an die Abmachung und nachdem die Duergar mithilfe der Gruppe den gesamten Tempel geplündert hatten wurde alles aufgehäuft was gefunden wurde. Das Gold war einfach geteilt allerdings die gefunden Gegenstände waren schwerer. 

Es wurde eine magische Lederrüstung, Platte, Langschwert, Kurzschwert sowie Dolch und Streitkolben gefunden. [Malgorn](../npc/malgorn.md) lies sich auf den Handel ein das die Duergar alle Waffen sowie die Lederrüstung bekommen während die Gruppe die wunderbar gearbeitet Platte bekommen würde. Besonders da die Rüstung aussah als währen sie aus feinen Platten gefertigt welche bei Bewegung keinerlei geräusch machten wäre sie perfekt für Jules geeignet.

Die Duergar schlugen auf den Handel ein und die Gruppe macht sich auf ihre Weiterreise.

## Weiterreise

Jules hatte ein ziemlich schlechtes Bauchgefühl die Platte anzuziehen und dies teilte sich der Gruppe mit das sie die Rüstung genauer untersuchen sollte als die offensichtlichen Vorteile die die Platte mit sich bringen würde.

Neldor konnte das Buch entschlüsseln was er bei Tela gefunden hatte und hier war ein magisches Ritual beschrieben welches die magischen Eigenschaften eines Gegenstandes enthüllen konnte. Mit diesem Ritual gelang es ihm zu erkennen das die Rüstung wirklich sehr mächtig war allerdings das auch ein Fluch auf der Rüstung liegt welcher beim anlegen der Rüstung mit Gift töten würde.

## Beute

| Gegenstand | Anzahl   | Bekommen von                | Charakter bekommen                          |
|-------------------|---|-----------------------------|---------------------------------------------|
| __83 gp, 3 sp__                   | 1 | versunkenem Tempel     | Bhalrik, Neldor, Jules, Eldeth, Severin, Hemeth   |
| Plate, Scorpion Armor             | 1 | versunkenem Tempel     | [Plop](../npc/plop.md) / Boote   |
| - | - | - | - |
| 1.000 gp                          | 1 | versunkener Tempel     | Duergar                          |
| Langschwert, of Lifestealing      | 1 | versunkener Tempel     | Duergar                          |
| Streitkolben, of Disturbing       | 1 | versunkener Tempel     | Duergar                          |
| Shortsword, +1                    | 1 | versunkener Tempel     | Duergar                          |
| Dagger, +1                        | 1 | versunkener Tempel     | Duergar                          |
| Leather, +1                       | 1 | versunkener Tempel     | Duergar                          |