## Pudding King


### geheimes fach
  - 55 gp, 30 ep; -> aufgteilt in gruppe.
  - 1 potion of posion -> sev
  - spell scrolls (conjure minor element -> bhalrik, speak with plants -> sev)
  - demon eye (crystal ball of true seeing) -> neldor (bag of holding)
  - stone of controlling earth elements -> bhalrik

## The first spellbook, titled Underland Magick, contains the following spells:

### 1st level

alarm, color spray, comprehend languages, find familiar, grease, identify, jump, Tasha’s hideous laughter, unseen servant

### 2nd level

alter self, blur, crown of madness, gust of wind, invisibility, knock, magic weapon, phantasmal force, spider climb


### 3rd level

blink, dispel magic, gaseous form, major image, protection from energy, slow, tongues, water breathing

### 4th level

blight, fabricate, fire shield, hallucinatory terrain, locate creature, phantasmal killer, polymorph, stoneskin

## The second spellbook, titled Magick from Beyond the Mirror, contains the following spells:

### 5th level: 

Bigby’s hand, cloudkill, hold monster, legend lore, passwall, Rary’s telepathic bond, telekinesis, wall of stone

### 6th level: 

chain lightning, disintegrate, Drawmij’s instant summons, eyebite, flesh to stone, Otto’s irresistible dance, true seeing
    
### 7th level: 

forcecage, mirage arcane, Mordenkainen’s magnificent mansion, prismatic spray, reverse gravity, teleport


### 8th level: 

antimagic field, feeblemind, incendiary cloud, maze, power word stun, telepathy


## Angriff der Drows

- Level Up! (Stufe 8)
- 6 short swords / 6 hand cross bows () -> bag of holding
- wand of viscid globs -> bag of holding