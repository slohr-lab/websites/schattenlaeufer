## Die Gefangennahme

Neldor kam wieder zu sich als er in Handschellen war und mit einer Kette an anderen Humanoiden gebunden war. Die Reise dauerte ein einige Tage und immer wieder ist er in eine Ummacht gefallen während er sich nach vorne geschleppt hatte. Er war erleichtert und besorgt zugleich als sie einen kleinen Außenposten seiner Peiniger, den Drow, erreicht hatten. Die Drow schenkten ihren Gefangenen wenig Beachtung solange sie sich ordentlich verhielten. Da er noch stark verwundet war von dem Kampf und es ihm schwer viel in der mit den Fesseln sich vernünftig zu bewegen hatte er die Peitsche mehr als einmal zu spüren bekommen. 

Der Außenposten, die Drow nennen ihn [Velkynvelve](/locations/velkynvelve.md), war an die Decker einer Höhle gebaut und von unten war sie fast vollkommen unsichtbar da sie durch massive spinnenweben verhüllt wurde. Lediglich die größeren Stalaktiten konnte man durch die dicken Spinnennetzte sehen. Die Höhle an sich war recht groß, hatte einen Wasserfall der einen kleineren See speiste und mehrere Ausgänge. Dies nahm [Neldor](/pc/neldor.md) lediglich am Rande seiner Wahrnehmung war als sie mit einem Lift nach oben gefahren wurden.

![Velkynvelve](/assets/images/log/session-01-01.png)

## Die Anderen

Oben angekommen musste sich [Neldor](/pc/neldor.md) komplett nackt ausziehen und ihm wurden lediglich seine Hose zurückgegeben damit er nicht komplett nackt war. Die Handschellen wurden ihm abgenommen und er wurde über mehrere Hängebrücken in eine Höhle geführt. Als [Neldor](/pc/neldor.md) die dicken Metallstäbe welche im Stein eingelassen waren sah wusste er das er sein vorläufiges Ziel erreicht hatten. Die Höhle war nicht leer, es befanden sich bereits andere Humanoide in der Zelle und es wurden noch mit denen befüllt welche mit einem Truppe ankamen. Mit ihm befanden sich mehrere Leute in Gefangenschaft was über die nächsten Tage noch etwas anwuchs. 

![Velkynvelve](/assets/images/log/session-01-02.png)

- [Buppido](/npc/buppido.md) ist ein älterer hagerer Derro der stehts mit aufmunternden Worten Charaktere tröstet und auch aktiv versucht Streitigkeiten zu unterbinden. Er scheint sehr intelligent zu sein. 
- [Prince Derendil](/npc/derendil.md) ist ein Elfenprinz welcher von einen mächtigen Magier in die Gestalt eines Quaggoth verwandelt wurde. Er spricht lediglich in Elfisch.
- [Eldeth Feldrun](/npc/eldeth.md) ist eine Schildzwergin aus [Gauntlgrym](/locations/gauntlgrym.md). Sie ist sturr und eine sehr gute Schmiedin.

![Velkynvelve](/assets/images/log/session-01-03.png)

- [Jimjar](/npc/jimjar.md) ist ein Tiefengnom der eine Obsession mit Wetten hat.
- [Ront](/npc/ront.md) ist ein Ork Bully wie er im buche steht. Aggressiv, dumm wie Stroh und hasst die Zwerge. Lässt keine Gelegenheit aus um die anderen Gefangenen zu schickanieren.
- [Sarith Kzekarit](/npc/sarith.md) ist ein sehr stiller und für sich zurückgezogener Dunkelelf. 

![Velkynvelve](/assets/images/log/session-01-04.png)

- [Shuushar the Awakend](/npc/shuushar.md) ist ein Kuo-Toa allerdings scheint er wirklich geistig erwacht zu sein da keine Anzeichen von Wahnsinns hat. 
- [Stool](/npc/stool.md) ist ein myconid (rasse von pilzwesen), welcher wie es scheint lediglich einzeln in der Zelle sitzt.
- [Topsy und Turvy](/npc/topsy_turvy.md) sind zwei Tiefengnome. Turvy murmelt sehr stark und ist nicht zu verstehen seine Zwillingsschwester Topsy übersetzt für ihn.

---

- [Jules](/pc/jules.md) sehr große und muskulöse Menschen Frau
- [Bhalrik](/pc/bhalrik.md) männlicher rothaariger Zwerg
- [Tela](/pc/tela.md) junge weibliche Tiefling Dame die ziemlich unsympathisch ist, steht in enger Beziehung mit [Tessaia](/pc/tessaia.md)
- [Tessaia](/pc/tessaia.md) weiblicher Tiefling welche ruhiger ist und die es wohl gewohnt ist Befehle zu geben, steht in enger Beziehung mit [Tela](/pc/tela.md)
- [Severin](/pc/severin.md) ein menschlicher Mann

## Überleben in Valkynvelve
Das tägliche Leben in Valkynvelve war es mit dem notwendigsten auszukommen und die Erniedrigungen der Drow Peiniger am besten zu überstehen. [Neldor](/pc/neldor.md) schnappte ein paar Informationen auf während er seine niederen Arbeiten verrichtet.

- Eine Drow Karawane mit Nachschub aus [Menzoberranzan](/locations/menzoberranzan.md) war ein paar Tage spät dran
- Den Ort (Quartier von [Ilvara](/npc/ilvara.md)) an dem die Ausrüstung der Gefangenen verwahrt wurde
- Einen groben Überblick über [Valkynvelve](/locations/velkynvelve.md)

Die Tage waren sehr beschwerlich und wenn alle Arbeiten im Außenposten erledigt waren machten sie die Drow meistens einen Spaß daraus die Leute welche einen kleinen Fehler gemacht hatten zu schlagen, quälen oder in einer anderen art und weise zu erniedrigen.

Des weiteren gelang es ihm einen gefleckten Edelstein zu entwenden und ihn in seinen Hosen zu verstecken das es keiner der Drow bemerkte. 

## Der Ausbruch
Nach mehreren Tagen der Gefangenschaft hatten die Gefangenen verschiedene Gegenstände zusammengestohlen damit sie einen Ausbruch wagen konnten. Als sie kurz darüber überlegten wie man dies am besten machen könnte kam [Jorlan](/npc/jorlan.md) auf die Gefangen zu und flüstere [Neldor](/pc/neldor.md) zu ob sie an einem Ausbruch planen würden. [Neldor](/pc/neldor.md) war sichtlich irritiert über die Aussage, hatte aber im Vorfeld schon Hoffnungen gehegt das der beste Weg aus der Gefangenschaft [Jorlan](/npc/jorlan.md) wäre, da dieser diskreditiert wurde und wohl am einfachsten zu überreden wäre die Gefangenen freizulassen damit man es [Ilvara](/npc/ilvara.md) etwas heimzahlen konnte.

[Jorlan](/npc/jorlan.md) erzählte das er dafür sorgen könne das die Zellentür unverschlossen sei und die Wachablösung etwas länger als gewöhnlich dauern könnte. Hierbei lies er den Gefangenen die Auswahl ob dies in der Früh oder am Abend passieren sollte. Die Gruppe entschied sich für den Abend und dann begann eine sehr hitzige Diskussion zwischen den Gefangenen was man wie machen sollte. [Tela](/pc/tela.md) wollte so schnell wie möglich von dem Außenposten fliehen und kein zusätzliches Risiko eingehen um die entwendeten Gegenstände zurückzuholen. [Bhalrik](/pc/bhalrik.md), [Neldor](/pc/neldor.md) wollten ihre Arkanen Fokus zurück holen da sie sonst stark bedingt Magie anwenden können. [Tela](/pc/tela.md) wurde schließlich von [Tessaia](/pc/tessaia.md) überredet das es eine gute Idee sei zu versuchen an die Gegenstände in der Truhe zu kommen da auch sie sonst nur stark eingeschränkt zu zaubern vermag. 

Die Diskussion zog sich noch mehrere Stunden hin wie man etwas machen konnte und [Neldor](/pc/neldor.md) verlor sehr schnell das Interesse an durchspielen aller möglichen Optionen, welche so vermutlich eh nie eintreffen würden. Er zog sich zurück und lege sich schlafen damit er ausgeruht war für den nächsten Morgen.

![Chasme](/assets/images/log/session-01-07.jpg){: align=right width=300}

Er wurde durch ein lautes Krachen gefolgt von einem Horn geweckt. Dies lies die immer noch andauernde "was wäre wenn" Diskussion ebenfalls verstummen. Ein lautes surrendes Geräusch erfüllte die gesamte Höhle und man konnte Rufe wahrnehmen. [Tela](/pc/tela.md) machte sich daran das schloss mit ihren improvisierten Dietrich zu öffnen und nach dem zweiten Versuch gelang es ihr auch. die Hälfte der Gefangen machte sich sofort durch das Spinnennetz auf den in das Wasser auf den Weg und versteckte sich so gut es geht in der großen höhle, während die anderen versuchen ihre verlorene Ausrüstung zu kommen.

Allen Anschein nach sind fliegende Dämonen in die Höhle eingedrungen und haben den Außenposten der Drow direkt angegriffen. Der Großteil der Drow war nun damit beschäftigt die Angreifer zurückzuschlagen und somit war der Weg zum WWachposten (mit angeschlossener Waffenkammer) unbewacht.

Severin, Bhalrik, [Neldor](../pc/neldor.md) und [Jules](../pc/jules.md) waren die ersten welche in den Rüstkammer der Drows kamen und anfingen sich zu bewaffnen und Rüstungen anzuziehen. Nach mehreren Minuten (als sie weitestgehend gerüstet waren) machten sie sich schleichend auf den Weg zum Schrein von Lolth und der daran angeschlossenen Kammer von [Ilvara](/npc/ilvara.md) wo sich ihre Gegenstände befanden. [Neldor](../pc/neldor.md) zog sich eine Brustplatte an nahm Langschwert und Schild mit soweit steckte sich einen Dolch in den Gürtel.

![Spider](/assets/images/log/session-01-06.jpg){: align=left width=300}

Beim Eintreffen im Schrein konnten sie sehen wie zwei Drow in die Kammer von [Ilvara](/npc/ilvara.md) gingen und eine Riesenspinne erspähen welche ide Gruppe noch nicht entdeckte und sie nutzten diese Gelegenheit um die Spinne schnell und leise auszuschalten. Die Kampf mit den Drow war ebenfalls schnell vorbei. [Neldor](../pc/neldor.md) wurde von ihrne Giftbolzen getroffen und ging kurz zu Boden bevor er von [Bhalrik](../pc/bhalrik.md) wieder aufgeweckt wurde. [Tela](../pc/tela.md) machte sich an der sehr großen Truhe zu schaffen und sie konnte das Schloss öffnen. Da die Drows keine tragbaren Behälter mochte musste sich die Gruppe aus den Umhängen und Bettbezügen behelfsmäßige Rucksäcke (ja Säcke ist hier wörtlich zu nehmen) bauen um die wichtigsten Gegenstände aus der Truhe mitzunehmen.

Neldor nahm sein wertvolles Buch, Arkanen Fokus, seine Reisekleidung und noch alle Wertgegenstände aus der Truhe an sich. Seinen Arkanen Fokus wieder an sich schickte er eine Nachricht an [Jules](../pc/jules.md) an die Zwergin Eldeth das die Truhe geöffnet ist und sie ihre kommen können um sich ihre Gegenstände zu holen, anschließend begab er sich zu [Severin](../pc/severin.md) welcher im Schrein von Lolth bereits angefangen hat die Edelstein Augen aus dem Spinnengötzen Bild zu entfernen. 

## Die Flucht
![Vrock](/assets/images/log/session-01-05.jpg){: align=right width=300}

Die Edelsteine eingepackt machte sich die Gruppe auf den Weg wieder zurück zum Wachturm wo die anderen Gefangenen bereits ein größeres Loch in das Spinnennetz geschnitten hatten und sprang mit einigen anderen Gefangenen in das Loch. [Tessaia](../pc/tessaia.md) zauberte Federfall damit die Gruppe leise und sanft in das Wasser landen konnte. Sie landeten im äußeren seichteren Teil des Sees und machten sich gerade dran aus dem Wasser zu steigen als ein großer stark verwundeter geflügelter Dämon bei ihnen landete. [Neldor](../pc/neldor.md) überlegte nicht lange und griff das Monster an. Leider stolperte er über einen Stein und viel zu Boden. Als er sich wieder aufgerichtet hatte war der Dämon bereits vernichtet. Er sah noch aus den Augenwinkeln wie die letzte Gruppe von Gefangenen im Wasser in ihrer nähe landeten.

Ein weiteres Hornsignal ertönte und [Neldor](../pc/neldor.md) konnte sehen wie einige der Drow in ihre Richtung zeigten bevor sie von sie gegen die angreifenden fliegenden Dämonen zur Wehr setzen mussten.

Die Gruppe der Gefangenen machte sich geschlossen auf den Weg nach Westen Richtung [Darklake](/locations/darklake.md).