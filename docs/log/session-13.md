## Geschenke

Bevor wir uns zur Nachtruhe begeben wollte ich mit der Gruppe meine Bemühungen mit der Gruppe teilen.

Ich übergab Jules eine [Feder](https://www.dndbeyond.com/magic-items/feather-token-feather-fall) welche sie langsam fallen lässt, da ich immer wieder gemerkt habe das sie massive Höhenangst hat und dazu noch eine [kleine Gewürztasche](https://www.dndbeyond.com/magic-items/hewards-handy-spice-pouch), welche ich mit der Hilfe von Esandra erstellen konnte. 

Für Bhalrik hatte ich ein [Zirkwood hergestellten Fokus](https://www.dndbeyond.com/magic-items/fernian-ash-focus) welcher seinen Feuerschaden erhöhen kann, da ich gemerkt habe das er sehr gerne Feuerbasierte Zauber spricht.

Einen schier [unzerbrechlichen Pfeil](https://www.dndbeyond.com/magic-items/unbreakable-arrow) habe ich für Severin übergeben damit er erstens magischen Schaden und zweiten theoretisch immer einen Pfeil überhat - solange er ihn sucht.


| Gegenstand                    | Person            | Kommentar                         | DDB-Link                          |
| ----------------------------- | ----------------- | --------------------------------- | --------------------------------- |
| Feather Token                 | Jules             | | https://www.dndbeyond.com/magic-items/feather-token-feather-fall    |
| Spice Pouch                   | Jules             | | https://www.dndbeyond.com/magic-items/hewards-handy-spice-pouch     |
| Ash Focus                     | Bhalrik           | | https://www.dndbeyond.com/magic-items/fernian-ash-focus             |
| Unbreakable Arrow             | Severin           | | https://www.dndbeyond.com/magic-items/unbreakable-arrow             |

## Gracklstugh

Am nächsten morgen (ich weiß es ist kein morgen, da wir hier keinen Orientierungspunkt für Zeit haben, für mich ist morgen wenn ich aufwache) fragten wir Hemth genauer über Gracklstugh aus und was uns dort erwartet.

Folgende Ratschläge gab er uns:

- sollen uns nicht mit Duergar anlegen
- wenn möglich nicht kämpfen
- wenn unbedingt gekämpft werden muss dann die Gegner bewusstlos schlagen (damit uns kein Mord vorgeworfen werden kann)
- Handelsviertel (Hafen) sollte sicher sein
- In die Zwergenstadt werden nur Duergar oder Personen mit Einladung reingelassen.

Bevor wir uns auf vorläufig die letzte Ettape über den Darklake machten versuchte ich noch mit Magie den Fluch, welcher von Lolth auf uns gelegt wurde, zu brechen und es gelang mir. 

### Ankuft im Hafenviertel

![Gracklstugh](../assets/images/locations/gracklstugh.jpg)

Die Luft wurde immer schlechter und wärmer je näher wir der Stadt kamen. Ein Geruch von Feuer, Geschmolzenem Metall erfüllte die Luft und wurde von von einem ständigen Hämmern tausender Hämmer begleitet.

Ich beschloss die Gestalt meines Duergar Alter-Egos anzunehmen da ich mich als Duergar bestimmt freier in der Stadt bewegen konnte.

Das Wasserlevel war ganz normal als wir in den Hafen einliefen und die dauerende Anspannung welche die letzten Wochen in Hemeth Gesicht geschrieben war, wich purer Freude. Mit unserem selbst gebauten und weitesgehend geflickten Boot legten wir an und Hemeth kümmerte sich um alle Formalitäten.

Während Jules, Severin, Sarith und Stool sich auf den Weg ins nächste Gasthaus begaben um dort für uns Zimmer für die nächsten Tage zu besorgen machten ich zusammen mit Hemeth, Bhalrik an die Arbeit die ganzen Waffen und Rüstungen welche wir erbeutet hatten zu verkaufen. Hemeth half hier sehr gut aus und organisierte uns einen eigenen Stand am Markt und binnen eines Tages konnten wir alles verkaufen was nicht magischen ursprungs war und gutes Geld dafür bekommen. 

Da wir abwechselnd am Stand waren konnte auch ich mich für ein paar Besorgungen umschauen und kaufte mehrere Gegenstände welche mir Nützlich erschienen für die Weiterreise im Underdark.

Mir vielen ziemlich viele merkwürdige Dinge in der Stadt auf. Immer mehr anzeigen eines sich ausbreitenden Wahnsinns machte sich bemerkbar. Mitten im Gespräch wurden Duergar unsichtbar. Sie redeten mit ihrem nicht vorhandenen Bruder oder änderten gar mit jedem Satz den Preis für den gleichen Gegenstand.

## Werz Saltbaron

Am späten Nachmittag beschlossen wir alle noch gemeinsam das Hafenviertel zu erkunden (da es alleine zu gefährlich sein könnte). Teilte mir Wisphers mit das zwei unsichtbare Duergar sich mit gezückten Waffen von hinten an den Duergar Händler anschlichen mit dem wir gerade sprachen.

Ich zog mein Schwert und stellte mich ihnen in den Weg und forderte sie auf von ihrer Absicht abzulassen. Sie hörten nicht und griffen mich un den Händler an. Nach zwei schnellen Schlägen sank der erste Duergar bewusstlos zu Boden während der zweite Duergar dne Händler seine klinge in den Bauch gerammt hatte. Severin heilte den Händler gleich und ich kümmerte mich um den noch verbliebenen Angreifer - während Jules nach den Wachen rief. 

Der Händler brach nur ein "scheiße, scheiße, scheiße" raus und bevor er weglief sagte er noch wir sollten uns im __Shattered Spire__ mit ihm in einigen Stunden treffen. 

Wir begaben uns direkt zur Taverne und warteten auf ihn. Jules kam ins Gespräch mit dem Wirt und handelte aus das sie für 20gp - vier Fässer Wasser jeden Tag für sie reinigen könnte - das heutige Essen und Trinken ging somit aufs Haus.

Wir tauschten uns ausgiebiger aus was wir den Tag so getan hatten. Jules erzählte das sie nachforschungen angestellt hat und wohl die beste Möglichkeit einen Guide nach Blingdenstone oder gar die Oberfläche zu finden mit Ylsa - einer der Zwerginen im Rat - zu sein scheint. 

Wenig später kam der Händler und stellte sich als Werz Saltbaron vor. Er erzählte uns das er einen Auftrag für uns hätte wenn wir nach Bligdenstone reisen würden. Er übergab uns einen Beutel von ungeschliffener Steine und diese sollten wir in Bligdenstone einer gewissen Kazook Pickshine übergeben. Neben Spell Gems pro Person würden wir auch 10pp pro Person bekommen.

Wir nahmen den Auftrag an.
