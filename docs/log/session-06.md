## Reise durch den Darklake

![Darklake](../assets/images/locations/darklake.jpg)

Nach dem stressigen entfernen von [Sloobludop](../locations/sloobludop.md) mit mehreren Stunden rudern hielt die Gruppe erst an als sie ziemlich erschöpft waren vom rudern. Sie fanden eine kleine Bucht und beschlossen hier zu rasten. [Tela](../pc/tela.md) und [Jules](../pc/jules.md) waren dafür unverzüglich weiterzureisen da sowohl der [Dämon](../npc/demogorgon.md) als auch [Buppido](../npc/buppido.md) ihnen auf den Fersen seien, sahen allerdings ein das sie alle Schlaf brauchten.

[Neldor](../pc/neldor.md) unterhielt sich mit [Hemeth](../npc/hemeth.md) und erfuhr das er ein für mehrere Karawanen von [Gracklstugh](../locations/gracklstugh.md) für den Geleitschutz zuständig ist und daher gut als Führer über den Darklake dienen kann. [Hemeth](../npc/hemeth.md) bedankte sich für die Rettung und auch das geliehene Schwert, Schild und die gehärtete Lederrüstung und versprach sich zu revanchieren wenn sie in seiner Heimat sind. 

Am nächsten Morgen verwendeten sie den kleinen Pilz als Wasserbehältnis und [Jules](../pc/jules.md) reinigte das Wasser in dem Pilz mit einem Zauberspruch so das es sicher zu trinken war. Niemand wollte weiter über das geschehene in [Sloobludop](../locations/sloobludop.md) sprechen und nach dem harschen Bemerkungen der anderen Gruppenmitglieder am vortag wollte [Neldor](../pc/neldor.md) den Stein nicht mehr ins Rollen bringen und beließ es auf sich.

### Aufgabenverteilung

|  Rudern   | Navigieren    | Nahrungssuche  | Ausschau |
| --------- | ------------- | -------------- |----------|
| Derendil  | Neldor        | Severin        | Bhalrik  |
| Tela      | Hemeth        | Eldeth         | Sarith   |
| Jules     |               |                |          |
| Tessaia   |               |                |          |

## Fischer

Die Reise am zweiten Tag war weitesgehend ereignislos lediglich konnte ihnen [Hemeth](../npc/hemeth.md) einmal aus helfen einen Wasserfall zu umfahren. Den zweiten Reisetag verbrachten sie auf dem Wasser und banden ihre Pilze sowie Floß an einem Stalagnaten an. Am dritten Tag der Reise kamen sie in eine größere Höhle und konnten die Nachtruhe auf einer größeren Gesteinsinsel verbringen. 

![Kuo-Toa](../assets/images/log/session-06/kuo-toa.jpg){: align=left width=300}

In der "früh" (als [Neldor](../pc/neldor.md), [Bhalrik](../pc/bhalrik.md) und [Eldeth](../npc/eldeth.md) Wache hatten) hörten sie ein Geräusch aus einiger Entfernung.  [Neldor](../pc/neldor.md) und [Bhalrik](../pc/bhalrik.md) beschlossen in die Richtung zu schleichen während [Eldeth](../npc/eldeth.md) das Lager bewachte und auf Nachricht warten sollte. Sie finden das eine Gruppe Kuo-Toa auf auf der Insel gelanden waren um ebenfalls rast zu machen. [Bhalrik](../pc/bhalrik.md) ging zum lager zurück um den Rest der Gruppe als potentielle verstärkung zu holen und [Neldor](../pc/neldor.md) ging auf die Kuo-Toa zu und versuchte mit Ihnen zu sprechen.

Es stellte sich heraus das die Kuo-Toa beim fischen unterwegs waren als ihr Boot gekentert ist (als sie von großen Fischwesen angegriffen wurden) und sie kamen dann auf ein herrenloses Kielboot der Duerger. Mit dem Boot möchten sie wieder in ihre Heimat [Sloobludop](../locations/sloobludop.md) zurück. Es stellte sich heraus das die Kuo-Toa anhänger der Seemutter sind und daher zeigten sie sich erleichtert als die Gruppe ihnen sagte sie seien Freunde von Ploopploopeen. Die Kuo-Toa zeigten sich bereit sich von ihrem unhandlichen Kielboot zu trennen und dafür einen der kleinen Pilze zu nehmen da es sich für sie deutlich einfacher damit navigieren lässt. 

Die zwei unterschiedlichen Gruppen führten den Tausch durch und die Kuo-Toa fühlten sich unwohl auf der Insel zu schlafen die bereits belegt ist und fuhren sofort weiter.

## Schiffsbau

![Boote](../assets/images/locations/oota_boats.png)

Der vierte Tag auf den Darklake verlief auch ziemlich ereignislos allerdings erreichten sie am als sie ihre Erschöpfungsgrenze kamen eine Insel mit einem Fungal-Wald. Es wurde beschlossen das man hier mit den vorhanden Materialien verschiedene Dinge herstellt. Das Floß wird in ein Katamaran umgebaut werden und zwei dichte Fässer (die als Wasservorrat dienen sollen) werden gefertigt. Dies dauert insgesamt einen Tag und die Gruppe verbringt somit auch den fünften Tag ihrer Reise über den Darklake damit ihre weitere Reise angenehmer und sicherer zu gestalten.

## Von Sandbänken und Zwergen

![Darkmantle](../assets/images/log/session-06/darkmantle.jpg){: align=right width=300}

Die nächsten Tage der Reise gestalten sich alle ereignislos und sicher. Die Gruppe hatte stehts ausreichend frisches Wasser und Nahrung zur Verfügung. Am achten Tag ihrer Reise erreichten sie eine Sandbank welche sich über den breiten Fluss spannte und die Gruppe musste ihre Boote über die Sandbank schieben. Hier wurden sie von vier Darkmantle angegriffen allerdings stellten sie die Gruppe keine Herausforderung dar, wenig später wurde die Gruppe während des Rasten in einem engeren Höhlensystem von mehreren Duergar angegriffen. Die Zwerge ignorierten alle Versuche den Konflikt friedlich zu lösen und somit kam es dann zu einem kurzen aber heftigen Kampf.