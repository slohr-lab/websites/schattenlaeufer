## Spieltrieb

![Worldmap](../assets/images/log/session-09/worldmap-begin.png)

Die weiterfahrt gestaltete sich als ruhig. Ruhes Wasser begünstige das vorankommen und wir konnten gut fahrt machen. Als wir müde wurden konnte eine kleie unbewohnte Insel ausfindig gemacht werden inmitten eines großen Sees und dort verbrachten die Gruppe eine lange Rast um am nächsten Morgen gestärkt aufzubrechen.

Von dem großen See fuhren wir ein eine etwas engere Passage mit etwas strömung und auch eineigen stromschnellen. Hier waren wir sehr froh das wir die Boote verstärkt hatten und die Vorräte angebunden haben damit hier nichts verrückt. Hier packten auch alle an zu Rudern da es ansonsten ziemlich eng gewesen worden wäre. 

Während wir durch einen engeren Tunnel machte Bhalrik sie aufmerksam auf in kleineres aufblitzeln in der Luft gefolgt von einem leichten kichern. Dies begleitete die Gruppe bis sie sich erneut zur Rast begaben. Da sie diesmal allerdings keine Insel zur Verfügung hatten. Beschlossen sie auf den Wasser zu ankern und zu ruhen. Wie jeden Tag vor dem einschlafen studierte Neldor neben seinem Buch auch das Buch was er bei [Tela](../pc/tela.md) gefunden hatte und bereits das identifizieren eines Gegestandes damit gelungen war. 

Das aufblitzen der Farben und Formen in der Entfernung war auch noch vorhanden als Neldor sich schlafen legen wollte und somit begann er ein weiteren Zauberspruch aus dem Buch zu wirken. Diesmal war es ein Schutzzuaber der die Boote einschloss und sollte irgendetwas auf die Boote kommen würde er durch ein lautes Geräusch in seinem Kopf geweeckt werden. Er fütterte Plop und und nutze dessen Bauchseite als Kissen und hüllte sich in seinen Reiseumhang ein den er noch etwas erwärmte und somit das gefühl einer warmen decke auf sich hatte während er ins Land der träume abglitt.

Neldor wusste nicht wie viel Zeit vergangen war, allerdings wurde er durch ein lautes Geräusch in seinem Kopf geweckt und er schreckte hoch. Er konnte an dem Gesicht von Jules erkennen dass etwas nicht stimmte und kurz darauf wurde er aufgeklärt das jemand sie wohl nicht schlafen lassen wollte und das kichern wie auch die lichtblitze immer näher kamen. Neldor er späte eine kleine schattenhafte gestalte und schickte einen Nachricht in die Richtung des Schattens. Er bekam sogar eine Antwort in einer Sprache die Neldor nur mit hilfe eines Zauber verstand. merkte er das das Wesen ihn wohl verstehen konnte aber beschloss nicht in seiner Sprache zu antworten. Durch seine arkane Erfahrung hatte er von solchen Feenwesen schon gehört, er zweifelte allerdings etwas an seinen Schlussfolgerungen da diese Wesen normalerweise in Wäldern und nicht im Underdark verweilten.

![Fina](../assets/images/log/session-09/pixie.png){: align=right width=300}

Während seine Gruppenmitglieder ebenfalls versuchen das Wesen hervor zu locken wartete Neldor bis er es wieder sah und schickte einen Zauber auf das Wesen was es zwang sich zu zeigen udn ihnen zu helfen. Dies zeigte Wirkung. Er bekam eine Antwort die nicht gerade glücklich über den Vorschlag sei - erschien allerdings auf dem Kopf von Plop. Es war eine Pixie mit dem Namen Fina. Wie sich herausstellte wollte sie nur spielen und die "doofen" Zwerge und Elfen tun dies nicht. 

Fina zeigte sich wenig erfreut das die Gruppe rasten wollte - bot allerdings an das sie in ihrem Dorf rasten könnte. Da das Dorf eine verlassene Ortschaft von Elfen sei wäre hier auch Platz für die Gruppe und es wäre nur wenige Stunden entfernt. Fina würde sie zum Dorf führen wenn sie ihr versprachen im Anschluss mit ihr und ihrem Stamm zu spielen. Die Gruppe willigte ein und Neldor legte sich wieder Schlafen während die Wachhabenden in Richtung Finas Dorf ruderten.

## verlassene Außenposten

![Pixiedorf](../assets/images/log/session-09/pixie_city.jpg){: align=right width=300}


Wie Fina versprochen hatte geleitete sie die Gruppe zu ihrem Dorf. Die Gruppe war angespannt und erwarteten eine Falle. Was sich allerdings als unnötig herausstellte. Als Neldor aufwachte waren sie bereits am Dorf und er begann die die verlassenen Häuser zu durchsuchen. Die wenigen Häuser lagen in einer mittelgroßen Höhle mit eigener Wasserversorgung welche in den Darklake floss. Auch gab es viele Pilze damit mehr als genug Nahrung für alle vorhanden war. Der eingang zum Dorf war sehr gut versteckt. Dies war der perfekte Ort für einen Außenposten im Darklake. 

Bei seiner Suche fand Neldor einige interessante Dinge. Es schien sich hierbei um einen, in eile, verlassenen Außenposten der Dunkelelfen zu handeln. In einem größeren Stalagmaten fand er mehrere arkane Utensilien. Auch waren darunter Zutaten dabei welche er vor einen Zauberspruch in dem Ritualbuch gefunden hatte. Neldor bereitete das Ritual vor und einige Pixies schauten interessiert zu. Besonders nachdem er ihnen gesagt hatte das er ihnen einen Spielgefährten beschwören würde.

### vertrauter Geist

![Whispers](../assets/images/characters/whispers.png){: align=left width=300}

Mit der gefundenen magischen Kreide zeichnete Neldor einen kleinen Beschwörungskreis auf den Steinboden und verbrannte ein paar Kräuter die er gefunden hatte und begann zu meditieren. Die Beschwörung dauerte eine gute Stunde in der Neldor in Gedanken nach einem Vertrauten suchte. Als das Ritual beendet war lag eine Katze auf seinem Schneidersitz und machte zeitgleich mit ihm die Augen auf. Die lila Augen trafen die Augen von Neldor und eine vertraute Intelligenz war darin zu sehen. Irgendetwas schien mit dem Körper der Katze nicht zu stimmen da sich sie Federn hatte. Neldor merkte das es sich hierbei um eine kleine schmale Katze handelte welche allerdings Flügel besaß. Das pechschwarze Fell des war mit silbernen Haaren durchzogen. 

Die Pixies welche nun wiederkamen, das Ritual hatte ihnen zu lange gedauert und sie hatten das interesse verloren, begutachteten das Katzenwesen. Whispers streckte sich erst und begann dann Neldor und die Pixies ab zu schnuppern. Bevor sie die Flügel spreizte und die Pixies voller Erwartung wegflogen und in der Luft zu schweben begannen. Whispers machte sich bereit ihnen zu folgen schaute dann Neldor in die Augen und er musste lächeln und nickte. mit einem rießigen Satz sprang Whispers in die Luft und verfolge die zwei Pixies. 

Neldor bemerkte das auch Bhalrik einen Falken beschworen hatte welcher mit den Pixies in der Luft fangen spielte und Jules spiele auch ein Spiel Fina.

Sie verbrachten einige Stunden bei den Pixies und auch als sie erschöpft waren und eingekuschelt mit Whispers auf Plop schliefen beratschlagte sich die Gruppe noch einmal. Severin hatte mehrere Edelsteine einen magischen Ring und weitere nützliche Gegenstände welche hier wohl zurückgelassen wurden. Hemeth war sehr besorgt über den niedrigen Wasserstand welcher sie schon seit mehreren Tagen begleitete und einige bekannte Strecken unpassierbar machte.

Nach der kurzen Schlafpause halfen die Pixies der Gruppe mit ihrer Magie die Boote als auch die von ihnen angefertigten Fesser zu verbessern damit sie deutlich wiederstandfähiger und gleichzeitig etwas leichter wurden. Auch halfen sie Neldor dabei seine Karte, welche er weiterhin vom Unterreich anfertigte, zu vervollständigen und mit wertvollen Informationen aus der unmittelbaren Umgebung zu befüllen.

## Brücken und Seile

![Brücken](../assets/images/log/session-09/bridges.jpg)

Die Gruppe machte sicher wieder auf den Weg. Sie entschlossen sich, da sie nun wieder volle Vorräter hatten, keine Nahrungssuche mehr zu machen sondern mehr Leute auf das rudern zu fokussieren um schneller voran zu kommen. Mit der Hilfestellung der Pixies konnten sie sich besser auf das niedrige Wasser einstellen und somit neue Routen finden. 

Am zweiten Tag ihrer Reise erreichten sie eine sehr hohe Höhle auf der es mehrere mit seegrass bewachsene Treppen gab welche auf einem großten Stalagmaten führten, welcher vor einem Wasserfall thronte. Hemeth erkannte die Höhle wieder, allerdings fehlten mehrere dutzend Meter an Wasser hier und, wie zu erwarten, mussten die Gruppe mitsamt den Booten auf den Stalagmaten hoch gebracht werden. Die Route ging weiter hinter dem Wasserfall; sie vergewisserten sich mit Whispers ob dies der Fall war und es konnte bestätigt werden das dort oben ein stehendes Gewässer ist welches "überschwappte" in Form eines Wasserfalles. 

Mit Steinverformungsmagie und ihren Seilen sowie viel Muskelkraft gelang es allen Beteiligten die Boote zu tragen, ziehen und zu heben bis sie schließlich an ihrem Ziel waren. Ein paar Gruppenmitglieder drohten abzustürzen konnten allerdings von ihrem Nachbarn gefangen werden und somit wurde niemand ernsthaft verletzt (abgesehen von ein paar Kratzern und ihrem Stolz). Neldor bemerkte das Jules ziemliche Schwierigkeiten hatte über eine hängebrücke, die über einen Felsspalt gespannt war, zu bewegen und dies obwohl sie Sicherungsseile angebracht hatten. Ihre knöchel waren weiß so fest klammerte sie sich an das Seil fest. 

Sie befestigen die Boote und legten eine lange Rast ein da sie von den Anstrengungen der letzten Stunden alle ziemlich erschöpft waren.

![Rochen](../assets/images/log/session-09/ixitxachitl.png){: align=right width=200}

## angriffslustige Rochen

Am nächsten Tag fuhren sie wieder über einen größeren See und er war bis auf große dämonische Mantarochen auch ereignislos. Sie stellten allerdings keine große bedrohung für die Gruppe da, da sie frühzeitig von Bhalrik erkannt wurden. 

Die Gruppe machte sich auf die Suche nach einem geeigneten Rastplatz für die Nacht.

## Beute

| Gegenstand | Anzahl   | Bekommen von                | Charakter bekommen                          |
|-------------------|---|-----------------------------|---------------------------------------------|
| __2 pp__                              | 1 | Pixie Dorf            | Severin                       |
| Emerald                               | 1 | Pixie Dorf            | Severin                       |
| Saphire (blue)                        | 1 | Pixie Dorf            | Severin                       |
| Amber                                 | 1 | Pixie Dorf            | Severin                       |
| Arkane Focus                          | 2 | Pixie Dorf            | Severin                       |
| - | - | - | - |
| Components for find familiar (10gp)   | 3 | Pixie Dorf            | Neldor                        |
| Components for new Spells (50gp)      | 15 | Pixie Dorf           | Neldor                        |
| - | - | - | - |
| fish tail inside a gilded acorn       | 1  | Pixie Dorf           | Bhalrik                       |
| - | - | - | - |
| ring (band of loyalty)                | 1  | Pixie Dorf           | Plop / Boot                   |

![Worldmap](../assets/images/log/session-09/worldmap-end.png)