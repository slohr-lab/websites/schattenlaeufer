## Charaktererstellung

### Attribute

Das Standard Array wurde etwas nach oben verändert und ist nun

| neu | alt |
|:---:|:---:|
| 17  | 15  |
| 15  | 13  |
| 14  | 12  |
| 13  | 11  |
| 12  | 10  |
| 10  | 8   |

!!! note "Beweggrund"

    Charakter sind Helden und mit dieser Regel werden die ersten Talente nicht für Attributserhöhungen ausgegeben sondern tragen weiter zur Individalisierung der Charaktere bei.

### Attributsveränderung durch die Rasse

Wenn es durch die ausgewählte Rasse zu einem Attributsbonus kommt so kann man 1 Wert auf ein anderes Attribut verlegen welches noch keinen Bonus bekommen hat.

=== "Richtig"

    __Standard:__

    |  |  | 
    |-:|:-|
    |__Rasse__          | Hügelzwerg    |
    |__Constitution__   | +2            |
    |__Wisdom__         | +1            |

    __Homebrew:__

    |  |  | 
    |-:|:-|
    |__Rasse__          | Hügelzwerg    |
    |__Constitution__   | +1            |
    |__Wisdom__         | +1            |
    |__Dexterity__      | +1            |

=== "Falsch"

    __Standard:__

    |  |  | 
    |-:|:-|
    |__Rasse__          | Hügelzwerg    |
    |__Constitution__   | +2            |
    |__Wisdom__         | +1            |

    __Homebrew:__

    |  |  | 
    |-:|:-|
    |__Rasse__          | Hügelzwerg    |
    |__Constitution__   | +1            |
    |__Wisdom__         | +2            |


!!! note "Beweggrund"

    Der Beweggrund hierfür ist das man durchaus andere Rassen ausprobieren kann die nicht perfekt für die ausgewählte KLasse passen. Immerhin gibt es ja auch druchaus variation unter uns Menschen und daher kann es durchaus sein das ein Zwerg auch deutlich geschickter ist wie der durchschnittle Zwerg.

## Aktionen

### Heiltränke

Der Konsum von Tränken dauert nach wie vor _1 Aktion_, allerdings heilen Heiltränke immer die volle Anzahl von Trefferpunkten. Sprich eine `Potion of Healing` heilt immer 10 HP und nicht 2d4+2.

!!! note "Beweggrund"
    
    Es soll eine taktische Handlung sein einen Trank zu trinken und auch anderen Fähigkeiten welchen dies als Bonus Action erlauben die Stärke beizubehalten.

## Hitpoints

### Würfeln

Hitpoints werden gewürfelt und bei einer gewürfelten 1 kann der Wurf wiederholt werden.

!!! note "Beweggrund"
    
    Charaktere sollen hierdurch etwas mehr individualisiert werden auch wenn es mathematisch der Durchschnitt wie im PHB angegeben ist.

## Kampf

### Kritischer Treffer

Bei einem kritischen Treffer verwenden wir die MAX Hit Rule. Diese bedeutet das der Schaden maximiert genommen wird und dazu dann noch alle Würfel einmal gewürfelt werden, so dass man bei einem kritischen Treffer immer mehr als den maximalen normalen Schaden machen kann.

!!! note "Beweggrund"
    
    Ein kritischer Treffer soll genau das sein. 