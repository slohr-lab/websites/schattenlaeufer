## Reisetage

| Tag       | Geschwindigkeit  | Spuren verwischen/Nahrung/Rudern   | Ausschau  | Navigation  | Bemerkung     |
| --------- | --------- | ------------------- | --------- | ----------- | ------------- |
| Tag 1     | schnell   | 13                  | 18        | 19          | Flucht von [Velkynvelve](../locations/velkynvelve.md) in [Session 02](../log/session-02.md) |
| Tag 2     | normal    | 21                  | 14        | 22          | Hooked Horror Hunt von [Session 02](../log/session-02.md)                                   |
| Tag 3     | normal    | 22                  | 21        | 15          | Dunkelelfenhändler [Session 03](../log/session-03.md)                                       |
| Tag 4     | schnell   | 8                   | 22        | 11          | Die Schlucht [Session 03](../log/session-03.md)                                             |
| Tag 5     | schnell   | 18                  | 8         | 12          | Ereignislos [Session 03](../log/session-03.md)                                              |
| Tag 6     | schnell   | 16                  | 19        | 8           | Ereignislos [Session 03](../log/session-03.md)                                              |
| Tag 7     | schnell   | 15                  | 17        | 11          | Opfergaben [Session 03](../log/session-03.md), [Session 04](../log/session-04.md)           |
| Tag 8     | normal    | 22                  | 19        | 5           | Sloobldop [Session 05](../log/session-05.md)                                                |
| Tag 9     | normal    | --                  | --        | --          | Darklake [Session 06](../log/session-06.md)                                                 |
| Tag 10    | normal    | 22                  | 22        | 22          | Darklake [Session 06](../log/session-06.md)                                                 |
| Tag 11    | normal    | 20                  | 15        | 23          | Kielboot erworben [Session 06](../log/session-06.md)                                        |
| Tag 12    | normal    | 19                  | 24        | 24          | Darklake [Session 06](../log/session-06.md)                                                 |
| Tag 13    | normal    | --                  | --        | --          | Crafting Island [Session 06](../log/session-06.md)                                          |
| Tag 14    | normal    | 25                  | 19        | 18          | Darklake [Session 06](../log/session-06.md)                                                 |
| Tag 15    | normal    | 25                  | 19        | 18          | Darklake [Session 06](../log/session-06.md)                                                 |
| Tag 16    | normal    | 25                  | 19        | 18          | Sandbank, Duergar [Session 06](../log/session-06.md) und Drow [Session 07](../log/session-07.md)    |
| Tag 17    | normal    | --                  | --        | --          | versunkener Tempel [Session 08](../log/session-08.md)                                       |
| Tag 18    | normal    | 24                  | 26        | 14          | ruhige reise (gut)                                                                          |
| Tag 19    | normal    | 24                  | 25        | 21          | pixie dorf                                                                                  |
| Tag 20    | normal    | --                  | 20        | 18          | ruhige reise (gut)                                                                          |
| Tag 21    | normal    | --                  | 19        | 22          | ruhige reise (sehr gut) / brücken                                                           |
| Tag 22    | normal    | --                  | 21        | 21          | manta dämonen fischlies / stromschnellen / fungal wald                                      |
| Tag 23    | normal    | --                  | --        | --          | Schifflebauen / Hags                                                                        |
| Tag 24    | normal    | --                  | --        | --          | Duergar Zwischenfall                  |
| Tag 25    | normal    | --                  | --        | --          | ruhige reise (gut)                    |
| Tag 26    | normal    | --                  | --        | --          | ruhige reise ruhige reise (gut)       |
| Tag 27    | normal    | --                  | --        | --          | ruhige reise (gut) / Lost Tomb        |
| Tag 28    | normal    | --                  | --        | --          | Ankunft [Gracklstugh](gracklstugh.md) / Waren verkauft |
| Tag 29    | normal    | --                  | --        | --          | Jules / Sev sind beeinflusst von der schlechten Luft |
| Tag 30    | normal    | --                  | --        | --          | Jules geht es besser, Sev schlechter / Hgraam, Ylsa getroffen |
| Tag 31    | normal    | --                  | --        | --          | Sev geht es besser |
| Tag 32    | normal    | --                  | --        | --          | Sev geht es wieder gut |
| Tag 33    | normal    | --                  | --        | --          | Sarith, Derendil zu Hgraam gebracht |
| Tag 34    | normal    | --                  | --        | --          |  |
| Tag 35    | normal    | --                  | --        | --          |  |
| Tag 36    | normal    | --                  | --        | --          |  |
| Tag 37    | normal    | --                  | --        | --          |  |
