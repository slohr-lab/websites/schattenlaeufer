## Karte

![Worldmap](../assets/images/log/session-12/worldmap.png)

## Reiseplan

Von [Velkynvelve](velkynvelve.md) reisen wir zum [Sloobludop](sloobludop.md) am [Darklake](darklake.md). Einmal dort angekommen suchen wir einen Führer über den Darklake nach [Gracklstugh](gracklstugh.md) vorbei am [Neverlight Grove](../locations/neverlight_grove.md) (dem Zuhause von [Stool](../npc/stool.md)). Von da aus können wir dann nach [Blingdenstone](blingdenstone.md) und von dort aus kommen finden wir an die Oberfläche.

``` mermaid
graph LR;

  A[Velkynvleve] ==> B[Sloobludop];

  subgraph Darklake
    B ==> D[Gracklstugh];
  end

  D ==> E[Neverlight Grove];
  E ==> F[Blingdenstone];
  F -.-> G[Oberfläche];
```
## Wache

Die lange Rast ist 1 Stunde länger (somit 9 anstatt 8 Stunden), diese extra Zeit soll für Reparieren und Crafting allgemein verwendet werden. 

| Wache 1       | Wache 2         | Wache 3       | Wache 4       |
|---------------|-----------------|---------------|---------------|
| Jules         |  Severin        | Neldor        | Bhalrik       |
| Eldeth        |  Derendil       | Sarith        | Hemeth        |

## Darklake

``` mermaid
graph TD;
   ELD[Eldeth];
   DER[Derendil];
   JUL[Jules];
   NEL[Neldor];
   BHA[Bhalrik];
   PLP[Plop];
   HEM[Hemeth];
   STO[Stool];
   SAR[Sarith];
   SEV[Severin];

  subgraph "Katamaran"
    DER ---- NEL
    NEL ---- HEM
    HEM ---- SAR
    SAR ---- PLP
  end

  subgraph "Kielboot"
    BHA ---- SEV
    SEV ---- JUL
    JUL ---- ELD
    ELD ---- STO
  end
```

### Aufgaben

|  Rudern   | Navigieren    | Nahrungssuche  | Ausschau |
| --------- | ------------- | -------------- |----------|
| Derendil  | Neldor        | Severin        | Bhalrik  |
| Jules     | Hemeth        | Eldeth         | Sarith   |

## Underdark

### Schmal

``` mermaid
graph LR;

  ELD[Eldeth];
  DER[Derendil];
  JUL[Jules];
  NEL[Neldor + Plop];
  BHA[Bhalrik];
  HEM[Hemeth];
  STO[Stool];
  SAR[Sarith];
  SEV[Severin];

  BHA ---- HEM
  HEM ---- NEL
  NEL ---- DER
  DER ---- SAR
  SAR ---- JUL
  JUL ---- STO
  STO ---- ELD
  ELD ---- SEV
```

### Breit

``` mermaid
graph LR;

  ELD[Eldeth];
  DER[Derendil];
  JUL[Jules];
  NEL[Neldor + Plop];
  BHA[Bhalrik];
  HEM[Hemeth];
  STO[Stool];
  SAR[Sarith];
  SEV[Severin];

  subgraph links
    BHA ---- HEM
    HEM ---- SAR
    SAR ---- JUL
    JUL ---- SEV
  end

  subgraph rechts
    NEL ---- DER
    DER ---- STO
    STO ---- ELD
  end
```

### Aufgaben

|  Spuren verwischen (Survival)         | Ausschau (Perception)  | Navigation  |
| ------------------------------------- | ---------------------- | ----------- |
| Severin                               | Bhalrik                | Neldor      |
| Eldeth                                | Hemeth                 | Sarith      |
