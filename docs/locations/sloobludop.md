![Sloobludop](/assets/images/locations/sloobludop.webp)

Sloobludop ist ein Kuo-toa-Dorf mit rund 500 Einwohner am Ostufer des Darklake im Underdark. Es erschien wie ein riesiges Gewirr aus Algen und Seetang, das von phosphoreszierenden Flecken beleuchtet wird und aus dem eine Reihe von Türmen herausragt, die von einem Gewirr aus Seil- und Plankenbrücken zusammengehalten werden.
 
Sloobludop erstreckt sich entlang des felsigen Ufers des Darklake, in einer Höhle, die von phosphoreszierenden Pilzen, Korallen und biolumineszierenden Fischdrüsen schwach beleuchtet wird. Die Dorfgrenzen wurden im Norden und Süden von hohen Zäunen aus gewebtem Netz umgeben. Das Dorf ist nach dem dreidimensionalen aquatischen Geometrieverständnis der Kuo-toa konstruiert, so dass es wie eine Unterwasserstruktur wirkt. Das Dorf ist von Brücken und Plattformen durchzogen, die die verschiedenen Ebenen der Strukturen miteinander verbanden. Die Brücken konnten ohne Schwierigkeiten überquert werden, aber die Struktur ist merklich schwach.

## Informationen

### Shuushar

Eher feindselig Fremden gegenüber. Er war Jahrelang nicht mehr da - es wurde die Seemutter angebetet. An sich sind die Kuo-Toa ein friedliebender Haufen. Bei Bedrohung lassen sich leicht aufscheuchen und gehen dann auch geschlossen und entschlossen gegen die Ursache vor. Eher weniger Schutz vor den Dunkelelfen da sie die direkte Konfrontation mit ihnen scheuen. Es leben primär Handwerker, Fischer, Jäger sowie Bootsbauer in der Stadt.

Wertgegenstände mit Wasserbezug ist interessant - Münzen werden zum Tausch akzeptiert.

### Kuo-Toa Gefangene

- [Leemooggoogoon](../npc/demogorgon.md) hat die Sea-Mother als Gott vor ein paar Monate abgelöst.
- Ploopploopeen (alter Stammesführer) ist nicht mehr von Bedeutung, verehrt immer noch die Sea-Mother.
- Bloppblippodd (Tochter von Ploopploopeen) ist Hohepriesterin von [Leemooggoogoon](../npc/demogorgon.md).