![Gracklstugh](../assets/images/locations/gracklstugh.jpg)

Gracklstugh ist eine Stadt welche in ein großes Höhlensystem des Underdarks gebaut wurde. Es hat mehrere Schmelzöfen sowie Schmieden und ist mit einem Hafen direkt an den [Darklake](darklake.md) gebaut. Die Luft riecht nach stark nach Feuer und Dampf während die Geräuschkulisse das Ringen von großen Hämmern auf Eisen erfüllt. Die großen Schmelzöfen erleuchten die Stadt und tauchen sie ein düsteres Rotes Leuchten, die 

Die Stadt selbst ist von gut 10.000 Duergar und Derro bewohnt welche Fremden nicht freundlichen eingestellt sind. Der Hafen ist direkt an das Ufer des Darklakes erbaut und hier sind auch viele unterschiedliche Rassen anzutreffen welche vor den großen Toren der Stadt Handel treiben. 

Die Dunkelelfen sind, wenn sie überhaupt vorkommen, sehr respektvoll gegenüber den Duerger und suchen keinen Streit.

In der Stadt sollten wir alle möglichen Handelswaren bekommen sowie sehr gute Güter aus Metal.