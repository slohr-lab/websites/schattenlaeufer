## Besatzung

- 2x Drow Pristerinnen
- 5x Elite Drow Krieger
- 19x Drow Krieger
- 6x trainierte Riesenspinnen
- 12x Quaggoth

## Wichtige Charaktere

| Name                  | Rasse         | Klasse                | Beschreibung                       |
| --------------------- | ------------- | --------------------  | ---------------------------------- |
| Ilvara                | Drow          | Priesterin (Lolth)    | Priesterin von Lolth und Anführerin des Außenposten |
| Asha                  | Drow          | Priesterin (Lolth)    | angehende Priesterin von Lloth, jung und ergeizig |
| Shoor                 | Drow          | Krieger               | Anführer der Drow,  Liebhaber von Ilvara, vor kurzem befördert |
| Jorlan                | Drow          | Krieger               | in einem kampf verwundetere Drow Krieger. Verkrüppelter Arm und schwere Narben im Gesicht. Ehemaliger Leutnant und Liebhaber von Ilvara |


![Velkynelve](/assets/images/locations/velkynvelve/map.jpg)

## Karte

### 1. Südlicher Wachposten

Hier sind rund um die Uhr zwei Drow anzutreffen welche ausschau halten ob jemand vom südlichen Ausgang der Höhle kommt.

### 2. Drow Barracken

Die Unterkünfte der Drow. In diesen zwei Höhlen stehen jeweils 6 Betten in der die Drow abwechselnd schalfen können.

### 3. Haupthalle

Im dieser Höhle nehmen die Drow und Quaggoth abwechselnd Nahrung auf. angeschlossen an die größeren Tische ist eine kleine Küche in der die Gefangenen das Essen zubereiten müssen.

### 4. Unterkünfte der Elite Drow

In den kleineren zwei Stalagtiten sind die kleine Behausungen einformt worden. In diesen Unterkünften haben die Elite kämpfer der Drow eigene Betten mit kleinen verschließbaren Truhen. [Jorlan](../npc/jorlan.md) ist nun ebenfalls hier stationiert.

### 5. Aufzug

Der Aufzug ist stehts von zwei Quaggoth bewacht und er wird auch von diesen zwei bedient falls es erfoderlich ist - was sehr recht selten ist. 

![Seitenansicht](/assets/images/locations/velkynvelve/sideview.jpg)

### 6. Schrein von  Lolth

In dem größten der Stalagtiten ist ganz oben ein Lolth geweiter Schrein erbaut. Eine große Spinne mit acht dunkel funkelnden Augen ist als Götze in die Wand eingelassen und die Decke ist mit dicken spinnweben überzogen. Hier man oft [Asha](../npc/asha.md) beten und ggf. auch ausruhen. Dieser Schrein hat eine direkte Verbindung mit [Ilvaras](../npc/ilvara.md) Unterkünfte. 

### 7. Ilvaras Unterkunft

Hier sind die Unterkünfte von [Ilvara](../npc/ilvara.md). Es ist ein sehr kleiner Raum als der Schrein aber es ist ein großes Bett, Schrank und eine große Truhe anzutreffen. Ebenso ein Abstieg zu [Shoors Unterkunft](../npc/shoor.md).

### 8. Shoors Unterkunft

Im untersten Raum im Stalagtit verbirgt sich die Unterkunft von [Shoor](../npc/shoor.md). Der Raum wird so gut wie nie benutzt und es war zuvor der Raum von [Jorlan](../npc/jorlan.md) welcher nun in den [Quartieren der Elite Drow](#4-unterkunfte-der-elite-drow) untergebracht ist.

### 9. Wasserfall

In der Mitte des Außenpostens ist ein Wasserfall welcher den kleinen See in der Höhle speißt. Der Wasserfall ist die firschwasserversorgung des Außenpostens und dienst zugleich zur entsorgung unerwünschter gegenstände / lebensmittel -> Welche alle in den See entsorgt werden.

### 10. Wachturm

Der Wachturm ist die einzige Verbindung zwischen dem nordlichen Teil des Außenposten und des südlichen. Im dem Wachposten sind meist zwei bis drei Drow anzutreffen und auch oberhalb des Wachpostens befindet sich auch die Waffenkammer mit einer kleinen Esse.

Hier haben sich mehrere Kettenhmenden, Kurzschwerter, Dolche sowie ein paar Burstplatten, Langschwerter, Handarmbrüste und eine vielzahl an Bolzen befunden.

### 11. Sklavenhöhle

Diese Höhle ist der einzige Ort im Außenposten welcher ein Schloss besitzt. Er wird Tag und Nacht von zwei Drow bewacht und dennoch ist die Tür mit einem Schloss versehen. Die Tür ist aus dicken Eisenstangen welche in die umliegende Felswand eingelassen sind. Diese Höhle ist mit einem einer Magie belegt die das Zaubern unmöglich macht. 

### 12. Quaggoth Unterkünfte

In dieser Höhle direkt neben den Gefangneen befindet sich die Unterkünftige der Quaggoth. Sie leben auch nicht viel besser als die Sklaven lediglich ist ihre Höhle nicht abgesperrt.

![Gray Ooze](/assets/images/locations/velkynvelve/grayooze.png){: align=right width=300}

### 13. Nördlicher Wachposten

Hier sind rund um die Uhr zwei Drow anzutreffen welche ausschau halten ob jemand vom nördlichen Ausgang der Höhle kommt.

### 14. Kleiner See

Dieser kleine See wird als Müllhalde für den Drow Außenposten verwendet. Dies hat dazugeführt das sich im See in grauer Ooze angesiedelt hat welcher sich von den Abfällen ernährt, die immer wieder in den See geworfen werden.