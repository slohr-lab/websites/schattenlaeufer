![Blingdenstone](/assets/images/locations/blingdenstone.jpg)

## Beschreibung

Am Eingang zur Stadt befanden sich eine Treppe und ein großes Eisentor. Gleich hinter dem Tor, im Inneren der Stadt, befand sich ein großes, verwinkeltes Labyrinth, das mögliche Eindringlinge aufhalten sollte. Die Stadt selbst bestand aus vielen Kavernenkammern, die durch gewundene Tunnel miteinander verbunden waren. Das Erscheinungsbild der Stadt war organisch, sanft in den Stein gemeißelt. Viele Gebäude erschienen als einfache Steinhaufen, waren aber in Wirklichkeit meisterhaft gemeißelt, um so zu erscheinen.

### Informationen von Turvy, Topsy und Jimjar

Hier wären wir ebenfalls in Sicherheit vor den Dunkelelfen. Die Tiefengnome sind, wie alle Rassen im Unterreich, nicht begeistert andere Rassen in ihre Stadt zu lassen. Der Großteil der Stadtbewohnern sind Händler und Künstler.