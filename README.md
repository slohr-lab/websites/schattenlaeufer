![Build Status](https://gitlab.com/slohr-lab/websites/schattenlaeufer/badges/master/pipeline.svg)

---

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:3.8-buster

before_script:
  - pip install -r requirements.txt

test:
  stage: test
  script:
  - mkdocs build --strict --verbose --site-dir test
  artifacts:
    paths:
    - test
  except:
  - master

pages:
  stage: deploy
  script:
  - mkdocs build --strict --verbose
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. clone or download this project
1. run dev-container
1. install requirements
    `pip install -r requirements.txt` 
1. run service
    `mkdocs serve`
1. webservice will become availble on port 8000
    `http://127.0.0.1:8000`
1. add content as you like

Read more at [MkDocs documentation](https://www.mkdocs.org/) and [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)

---

Forked from https://gitlab.com/morph027/mkdocs
